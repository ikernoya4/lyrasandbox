// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Player/LyraPlayerBotController.h"
#include "SandboxGOAPController.generated.h"

/**
 * 
 */
UCLASS()
class LYRAGAME_API ASandboxGOAPController : public ALyraPlayerBotController
{
	GENERATED_BODY()
	/*
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	UPlannerComponent* Planner;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FGoapAction> Actions;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<FSubGoal, bool> Goals;
	*/
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class ASandboxCover* CurrentCover;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bCanAttack = true;
};
