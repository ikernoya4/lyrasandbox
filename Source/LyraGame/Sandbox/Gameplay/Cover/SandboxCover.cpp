// Fill out your copyright notice in the Description page of Project Settings.


#include "Sandbox/Gameplay/Cover/SandboxCover.h"

#include "Character/LyraHealthComponent.h"
#include "Components/BillboardComponent.h"
#include "Engine/Selection.h"

ASandboxCover::ASandboxCover()
{
	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	SetRootComponent(Box);
	SubdivideCover();
}

AActor* ASandboxCover::GetCoverOwner(FVector Location)
{
	if (CoverPosition.Contains(Location))
	{
		return CoverPosition[Location];
	}
	return nullptr;
}

TArray<FVector> ASandboxCover::GetCoverLocations()
{
	TArray<FVector> Locations;
	CoverPosition.GetKeys(Locations);
	return Locations;
}

FVector ASandboxCover::GetReservedPositionByOwner(AActor* CurrentOwner)
{
	if (CurrentOwner)
	{
		TArray<FVector> Keys;
		CoverPosition.GetKeys(Keys);
		for (FVector Key : Keys)
		{
			if (CoverPosition[Key] && CoverPosition[Key] == CurrentOwner)
			{
				return Key;
			}
		}
	}

	return FVector::ZeroVector;
}

bool ASandboxCover::CheckIfPositionIsTaken(FVector CoverLocation)
{
	return CoverPosition.Contains(CoverLocation) && CoverPosition[CoverLocation] != nullptr;
}

bool ASandboxCover::IsThereFreeCover()
{
	TArray<FVector> Keys;
	CoverPosition.GetKeys(Keys);
	for (FVector Key : Keys)
	{
		if (!CheckIfPositionIsTaken(Key))
		{
			return true;
		}
	}
	return false;
}

bool ASandboxCover::IsActorAnOwner(AActor* Actor)
{
	for (FVector Key : GetCoverLocations())
	{
		if (CoverPosition.Contains(Key) && CoverPosition[Key] == Actor)
		{
			return true;
		}
	}
	return false;
}

void ASandboxCover::ReserveCover(FVector Location, ACharacter* NewOwner)
{
	if(NewOwner)
	{
		if (CoverPosition.Contains(Location) && CoverPosition[Location] == nullptr)
		{
			CoverPosition[Location] = NewOwner;
		}
		if(auto* HealthComponent = Cast<ULyraHealthComponent>(NewOwner->GetComponentByClass(ULyraHealthComponent::StaticClass())))
		{
			if(!HealthComponent->OnDeathStarted.Contains(this, FreeCoverFunctionName))
			{
				HealthComponent->OnDeathStarted.AddDynamic(this, &ASandboxCover::FreeCover);
			}
		}

	}
}

void ASandboxCover::FreeCover(AActor* PreviousOwner)
{
	if (PreviousOwner)
	{
		TArray<FVector> Keys = GetCoverLocations();
		if(Keys.Num()>0)
		{
			for (FVector Key : Keys)
			{
				if (CoverPosition.Contains(Key) && CoverPosition[Key] && CoverPosition[Key] == PreviousOwner)
				{
					CoverPosition[Key] = nullptr;
				}
			}
			if (auto* Character = Cast<ALyraCharacter>(PreviousOwner))
			{
				if (auto* HealthComponent = Cast<ULyraHealthComponent>(
					Character->GetComponentByClass(ULyraHealthComponent::StaticClass())))
				{
					if (HealthComponent->OnDeathStarted.Contains(this, FreeCoverFunctionName))
					{
						HealthComponent->OnDeathStarted.Remove(this, FreeCoverFunctionName);
					}
				}
				Character->UnCrouch();
			}
		}
		
	}
}

void ASandboxCover::FreeCover(FVector Position)
{
	if (CoverPosition.Contains(Position) && CoverPosition[Position])
	{
		CoverPosition[Position] = nullptr;
	}
}

void ASandboxCover::SubdivideCover()
{
	CoverPosition.Empty();
	for (UBillboardComponent* Sprite : CoverPositionSprite)
	{
		if (Sprite)
		{
			Sprite->DestroyComponent();
		}
	}
	CoverPositionSprite.Empty();
	float BoxExtentX = Box->GetScaledBoxExtent().X;
	int MaxSubdivisionCount = static_cast<int>(BoxExtentX) / static_cast<int>(AIRadius);
	if (CoverSubdivisionCount > MaxSubdivisionCount)
	{
		CoverSubdivisionCount = MaxSubdivisionCount;
	}
	float BoxExtentDivision = BoxExtentX / CoverSubdivisionCount;
	FVector InitialLocation = FVector(-BoxExtentX + (BoxExtentDivision), 0, 0);
	float Offset = 0;
	for (int i = 0; i < CoverSubdivisionCount; i++)
	{
		int id = FMath::RandRange(0, 1000);
		FName Name = FName(*FString::Printf(TEXT("Sprite %d "), id));
		FVector NewPosition = FVector(InitialLocation + FVector(Offset, 0, 0));
		if (auto* Sprite = NewObject<UBillboardComponent>(this, UBillboardComponent::StaticClass(), Name))
		{
			Sprite->SetupAttachment(Box);
			Sprite->SetRelativeLocation(NewPosition);
			Sprite->SetRelativeRotation(FRotator::ZeroRotator);
			Sprite->SetSprite(CoverPositionTexture);
			Sprite->SetVisibility(true);
			Sprite->RegisterComponent();
			CoverPosition.Add(Sprite->GetComponentLocation(), nullptr);
			CoverPositionSprite.Add(Sprite);
		}
		Offset += BoxExtentDivision * 2;
	}
}

void ASandboxCover::CheckCoverType()
{
	if (Box->GetScaledBoxExtent().Z < SizeNeededForFullCover)
	{
		Type = ECoverTypes::Partial;
	}
	else
	{
		Type = ECoverTypes::Full;
	}
}

void ASandboxCover::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
	CheckCoverType();
	SubdivideCover();
}
