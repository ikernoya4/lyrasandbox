// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Character/LyraCharacter.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "SandboxCover.generated.h"

class UBillboardComponent;

UENUM(BlueprintType)
enum class ECoverTypes : uint8
{
	Full,
	Partial
};


UCLASS()
class LYRAGAME_API ASandboxCover : public AActor
{
	GENERATED_BODY()
	int32 CurrentDivisionCount = 0;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Component")
	UBoxComponent* Box;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	TArray<UBillboardComponent*> CoverPositionSprite;
	UPROPERTY(VisibleAnywhere, Category = "Cover")
	//   Position     Owner
	TMap<FVector, AActor*> CoverPosition;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cover")
	float SizeNeededForFullCover = 100.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cover")
	float AIRadius = 34.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cover", meta = (ClampMin = 1), meta = (ClampMax = 10))
	int32 CoverSubdivisionCount = 1;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Cover")
	ECoverTypes Type = ECoverTypes::Partial;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cover")
	UTexture2D* CoverPositionTexture;

	UPROPERTY()
	FName FreeCoverFunctionName = "FreeCover";
public:
	ASandboxCover();
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Cover")
	FORCEINLINE ECoverTypes GetCoverType() { return Type; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Cover")
	AActor* GetCoverOwner(FVector Location);
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Cover")
	TArray<FVector> GetCoverLocations();
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Cover")
	FVector GetReservedPositionByOwner(AActor* CurrentOwner);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Cover")
	bool CheckIfPositionIsTaken(FVector CoverLocation);
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Cover")
	bool IsThereFreeCover();
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Cover")
	bool IsActorAnOwner(AActor* Actor);

	UFUNCTION(BlueprintCallable, Category = "Cover")
	void FreeCover(AActor* PreviousOwner);	
	void FreeCover(FVector Position);

	void ReserveCover(FVector Location, ACharacter* NewOwner);
private:
	
	void SubdivideCover();
	void CheckCoverType();
	
	virtual void OnConstruction(const FTransform& Transform) override;
};
