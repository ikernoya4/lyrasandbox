// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AIRuntime/Private/AITestSpawningManagmentComponent.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAITestSpawningManagmentComponent() {}
// Cross Module References
	AIRUNTIME_API UClass* Z_Construct_UClass_UAITestSpawningManagmentComponent_NoRegister();
	AIRUNTIME_API UClass* Z_Construct_UClass_UAITestSpawningManagmentComponent();
	LYRAGAME_API UClass* Z_Construct_UClass_ULyraPlayerSpawningManagerComponent();
	UPackage* Z_Construct_UPackage__Script_AIRuntime();
// End Cross Module References
	void UAITestSpawningManagmentComponent::StaticRegisterNativesUAITestSpawningManagmentComponent()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UAITestSpawningManagmentComponent);
	UClass* Z_Construct_UClass_UAITestSpawningManagmentComponent_NoRegister()
	{
		return UAITestSpawningManagmentComponent::StaticClass();
	}
	struct Z_Construct_UClass_UAITestSpawningManagmentComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_PlayerStartTag_MetaData[];
#endif
		static const UECodeGen_Private::FNamePropertyParams NewProp_PlayerStartTag;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_BotStartTag_MetaData[];
#endif
		static const UECodeGen_Private::FNamePropertyParams NewProp_BotStartTag;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAITestSpawningManagmentComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ULyraPlayerSpawningManagerComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_AIRuntime,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAITestSpawningManagmentComponent_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "AITestSpawningManagmentComponent.h" },
		{ "ModuleRelativePath", "Private/AITestSpawningManagmentComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAITestSpawningManagmentComponent_Statics::NewProp_PlayerStartTag_MetaData[] = {
		{ "Category", "AITest|Spawner" },
		{ "ModuleRelativePath", "Private/AITestSpawningManagmentComponent.h" },
	};
#endif
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UClass_UAITestSpawningManagmentComponent_Statics::NewProp_PlayerStartTag = { "PlayerStartTag", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAITestSpawningManagmentComponent, PlayerStartTag), METADATA_PARAMS(Z_Construct_UClass_UAITestSpawningManagmentComponent_Statics::NewProp_PlayerStartTag_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAITestSpawningManagmentComponent_Statics::NewProp_PlayerStartTag_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAITestSpawningManagmentComponent_Statics::NewProp_BotStartTag_MetaData[] = {
		{ "Category", "AITest|Spawner" },
		{ "ModuleRelativePath", "Private/AITestSpawningManagmentComponent.h" },
	};
#endif
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UClass_UAITestSpawningManagmentComponent_Statics::NewProp_BotStartTag = { "BotStartTag", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAITestSpawningManagmentComponent, BotStartTag), METADATA_PARAMS(Z_Construct_UClass_UAITestSpawningManagmentComponent_Statics::NewProp_BotStartTag_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAITestSpawningManagmentComponent_Statics::NewProp_BotStartTag_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAITestSpawningManagmentComponent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAITestSpawningManagmentComponent_Statics::NewProp_PlayerStartTag,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAITestSpawningManagmentComponent_Statics::NewProp_BotStartTag,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAITestSpawningManagmentComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAITestSpawningManagmentComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UAITestSpawningManagmentComponent_Statics::ClassParams = {
		&UAITestSpawningManagmentComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAITestSpawningManagmentComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAITestSpawningManagmentComponent_Statics::PropPointers),
		0,
		0x00A000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UAITestSpawningManagmentComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAITestSpawningManagmentComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAITestSpawningManagmentComponent()
	{
		if (!Z_Registration_Info_UClass_UAITestSpawningManagmentComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UAITestSpawningManagmentComponent.OuterSingleton, Z_Construct_UClass_UAITestSpawningManagmentComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UAITestSpawningManagmentComponent.OuterSingleton;
	}
	template<> AIRUNTIME_API UClass* StaticClass<UAITestSpawningManagmentComponent>()
	{
		return UAITestSpawningManagmentComponent::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAITestSpawningManagmentComponent);
	struct Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Private_AITestSpawningManagmentComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Private_AITestSpawningManagmentComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UAITestSpawningManagmentComponent, UAITestSpawningManagmentComponent::StaticClass, TEXT("UAITestSpawningManagmentComponent"), &Z_Registration_Info_UClass_UAITestSpawningManagmentComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UAITestSpawningManagmentComponent), 1086414999U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Private_AITestSpawningManagmentComponent_h_2345460965(TEXT("/Script/AIRuntime"),
		Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Private_AITestSpawningManagmentComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Private_AITestSpawningManagmentComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
