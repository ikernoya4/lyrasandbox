// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FWorldStates;
#ifdef AIRUNTIME_AIWorldState_generated_h
#error "AIWorldState.generated.h already included, missing '#pragma once' in AIWorldState.h"
#endif
#define AIRUNTIME_AIWorldState_generated_h

#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_AIWorldState_h_10_DELEGATE \
struct _Script_AIRuntime_eventOnStateChanged_Parms \
{ \
	FName StateName; \
	bool Value; \
}; \
static inline void FOnStateChanged_DelegateWrapper(const FMulticastScriptDelegate& OnStateChanged, FName StateName, bool Value) \
{ \
	_Script_AIRuntime_eventOnStateChanged_Parms Parms; \
	Parms.StateName=StateName; \
	Parms.Value=Value ? true : false; \
	OnStateChanged.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_AIWorldState_h_15_SPARSE_DATA
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_AIWorldState_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execModifyState); \
	DECLARE_FUNCTION(execGetWorldState);


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_AIWorldState_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execModifyState); \
	DECLARE_FUNCTION(execGetWorldState);


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_AIWorldState_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAIWorldState(); \
	friend struct Z_Construct_UClass_UAIWorldState_Statics; \
public: \
	DECLARE_CLASS(UAIWorldState, UGameStateComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AIRuntime"), NO_API) \
	DECLARE_SERIALIZER(UAIWorldState)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_AIWorldState_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUAIWorldState(); \
	friend struct Z_Construct_UClass_UAIWorldState_Statics; \
public: \
	DECLARE_CLASS(UAIWorldState, UGameStateComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AIRuntime"), NO_API) \
	DECLARE_SERIALIZER(UAIWorldState)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_AIWorldState_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAIWorldState(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAIWorldState) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAIWorldState); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAIWorldState); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAIWorldState(UAIWorldState&&); \
	NO_API UAIWorldState(const UAIWorldState&); \
public:


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_AIWorldState_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAIWorldState(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAIWorldState(UAIWorldState&&); \
	NO_API UAIWorldState(const UAIWorldState&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAIWorldState); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAIWorldState); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAIWorldState)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_AIWorldState_h_12_PROLOG
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_AIWorldState_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_AIWorldState_h_15_SPARSE_DATA \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_AIWorldState_h_15_RPC_WRAPPERS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_AIWorldState_h_15_INCLASS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_AIWorldState_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_AIWorldState_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_AIWorldState_h_15_SPARSE_DATA \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_AIWorldState_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_AIWorldState_h_15_INCLASS_NO_PURE_DECLS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_AIWorldState_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AIRUNTIME_API UClass* StaticClass<class UAIWorldState>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_AIWorldState_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
