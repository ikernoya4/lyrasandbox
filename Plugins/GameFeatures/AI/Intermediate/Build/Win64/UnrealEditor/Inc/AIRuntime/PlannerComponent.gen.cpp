// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AIRuntime/Public/Components/PlannerComponent.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePlannerComponent() {}
// Cross Module References
	AIRUNTIME_API UClass* Z_Construct_UClass_UPlannerComponent_NoRegister();
	AIRUNTIME_API UClass* Z_Construct_UClass_UPlannerComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_AIRuntime();
	AIRUNTIME_API UClass* Z_Construct_UClass_UGActionBase_NoRegister();
	AIRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FWorldStates();
	AIRUNTIME_API UClass* Z_Construct_UClass_UGNode_NoRegister();
	AIRUNTIME_API UClass* Z_Construct_UClass_UGNode();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
	DEFINE_FUNCTION(UPlannerComponent::execPlan)
	{
		P_GET_TARRAY(UGActionBase*,Z_Param_Actions);
		P_GET_TMAP(FName,bool,Z_Param_Goal);
		P_GET_STRUCT(FWorldStates,Z_Param_States);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UGActionBase*>*)Z_Param__Result=P_THIS->Plan(Z_Param_Actions,Z_Param_Goal,Z_Param_States);
		P_NATIVE_END;
	}
	void UPlannerComponent::StaticRegisterNativesUPlannerComponent()
	{
		UClass* Class = UPlannerComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Plan", &UPlannerComponent::execPlan },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UPlannerComponent_Plan_Statics
	{
		struct PlannerComponent_eventPlan_Parms
		{
			TArray<UGActionBase*> Actions;
			TMap<FName,bool> Goal;
			FWorldStates States;
			TArray<UGActionBase*> ReturnValue;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Actions_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_Actions;
		static const UECodeGen_Private::FBoolPropertyParams NewProp_Goal_ValueProp;
		static const UECodeGen_Private::FNamePropertyParams NewProp_Goal_Key_KeyProp;
		static const UECodeGen_Private::FMapPropertyParams NewProp_Goal;
		static const UECodeGen_Private::FStructPropertyParams NewProp_States;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UPlannerComponent_Plan_Statics::NewProp_Actions_Inner = { "Actions", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UGActionBase_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UPlannerComponent_Plan_Statics::NewProp_Actions = { "Actions", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PlannerComponent_eventPlan_Parms, Actions), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPlannerComponent_Plan_Statics::NewProp_Goal_ValueProp = { "Goal", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UPlannerComponent_Plan_Statics::NewProp_Goal_Key_KeyProp = { "Goal_Key", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UPlannerComponent_Plan_Statics::NewProp_Goal = { "Goal", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PlannerComponent_eventPlan_Parms, Goal), EMapPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPlannerComponent_Plan_Statics::NewProp_States = { "States", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PlannerComponent_eventPlan_Parms, States), Z_Construct_UScriptStruct_FWorldStates, METADATA_PARAMS(nullptr, 0) }; // 21332049
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UPlannerComponent_Plan_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UGActionBase_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UPlannerComponent_Plan_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PlannerComponent_eventPlan_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPlannerComponent_Plan_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPlannerComponent_Plan_Statics::NewProp_Actions_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPlannerComponent_Plan_Statics::NewProp_Actions,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPlannerComponent_Plan_Statics::NewProp_Goal_ValueProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPlannerComponent_Plan_Statics::NewProp_Goal_Key_KeyProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPlannerComponent_Plan_Statics::NewProp_Goal,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPlannerComponent_Plan_Statics::NewProp_States,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPlannerComponent_Plan_Statics::NewProp_ReturnValue_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPlannerComponent_Plan_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPlannerComponent_Plan_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Components/PlannerComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UPlannerComponent_Plan_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPlannerComponent, nullptr, "Plan", nullptr, nullptr, sizeof(Z_Construct_UFunction_UPlannerComponent_Plan_Statics::PlannerComponent_eventPlan_Parms), Z_Construct_UFunction_UPlannerComponent_Plan_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlannerComponent_Plan_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPlannerComponent_Plan_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlannerComponent_Plan_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPlannerComponent_Plan()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UPlannerComponent_Plan_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UPlannerComponent);
	UClass* Z_Construct_UClass_UPlannerComponent_NoRegister()
	{
		return UPlannerComponent::StaticClass();
	}
	struct Z_Construct_UClass_UPlannerComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_bShouldFormulatePlan_MetaData[];
#endif
		static void NewProp_bShouldFormulatePlan_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bShouldFormulatePlan;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPlannerComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_AIRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UPlannerComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UPlannerComponent_Plan, "Plan" }, // 1788356653
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlannerComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "Components/PlannerComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Components/PlannerComponent.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlannerComponent_Statics::NewProp_bShouldFormulatePlan_MetaData[] = {
		{ "Category", "PlannerComponent" },
		{ "ModuleRelativePath", "Public/Components/PlannerComponent.h" },
	};
#endif
	void Z_Construct_UClass_UPlannerComponent_Statics::NewProp_bShouldFormulatePlan_SetBit(void* Obj)
	{
		((UPlannerComponent*)Obj)->bShouldFormulatePlan = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPlannerComponent_Statics::NewProp_bShouldFormulatePlan = { "bShouldFormulatePlan", nullptr, (EPropertyFlags)0x0010000000020005, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPlannerComponent), &Z_Construct_UClass_UPlannerComponent_Statics::NewProp_bShouldFormulatePlan_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPlannerComponent_Statics::NewProp_bShouldFormulatePlan_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlannerComponent_Statics::NewProp_bShouldFormulatePlan_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPlannerComponent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlannerComponent_Statics::NewProp_bShouldFormulatePlan,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPlannerComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPlannerComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UPlannerComponent_Statics::ClassParams = {
		&UPlannerComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UPlannerComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UPlannerComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UPlannerComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPlannerComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPlannerComponent()
	{
		if (!Z_Registration_Info_UClass_UPlannerComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UPlannerComponent.OuterSingleton, Z_Construct_UClass_UPlannerComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UPlannerComponent.OuterSingleton;
	}
	template<> AIRUNTIME_API UClass* StaticClass<UPlannerComponent>()
	{
		return UPlannerComponent::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPlannerComponent);
	void UGNode::StaticRegisterNativesUGNode()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UGNode);
	UClass* Z_Construct_UClass_UGNode_NoRegister()
	{
		return UGNode::StaticClass();
	}
	struct Z_Construct_UClass_UGNode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Parent_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Parent;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Cost_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_Cost;
		static const UECodeGen_Private::FBoolPropertyParams NewProp_State_ValueProp;
		static const UECodeGen_Private::FNamePropertyParams NewProp_State_Key_KeyProp;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_State_MetaData[];
#endif
		static const UECodeGen_Private::FMapPropertyParams NewProp_State;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Action_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Action;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGNode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_AIRuntime,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGNode_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// USTRUCT(BlueprintType)\n// struct FGNode\n// {\n// \x09GENERATED_BODY()\n// \x09\n// \x09""FGNode Parent;\n// \x09\n// \x09UPROPERTY(EditAnywhere, BlueprintReadWrite)\n// \x09""float Cost = 0;\n// \x09UPROPERTY(EditAnywhere, BlueprintReadWrite)\n// \x09TMap<FName, bool> State;\n// \x09UPROPERTY(EditAnywhere, BlueprintReadWrite)\n// \x09""FGoapAction Action;\n//\n// \x09""FGNode(){}\n// \x09""FGNode(FGNode& NewParent, float NewCost, TMap<FName, bool> AllStates, FGoapAction NewAction);\n// \x09""bool IsNull();\n// \x09void operator=(const FGNode& Other) \n// \x09{\n// \x09\x09if(!Other.Parent.IsNull()) Parent = Other.Parent;\n// \x09\x09""Cost = Other.Cost;\n// \x09\x09State = Other.State;\n// \x09\x09""Action = Other.Action;\n// \x09}\n// };\n" },
		{ "IncludePath", "Components/PlannerComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Components/PlannerComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "USTRUCT(BlueprintType)\nstruct FGNode\n{\n      GENERATED_BODY()\n\n      FGNode Parent;\n\n      UPROPERTY(EditAnywhere, BlueprintReadWrite)\n      float Cost = 0;\n      UPROPERTY(EditAnywhere, BlueprintReadWrite)\n      TMap<FName, bool> State;\n      UPROPERTY(EditAnywhere, BlueprintReadWrite)\n      FGoapAction Action;\n\n      FGNode(){}\n      FGNode(FGNode& NewParent, float NewCost, TMap<FName, bool> AllStates, FGoapAction NewAction);\n      bool IsNull();\n      void operator=(const FGNode& Other)\n      {\n              if(!Other.Parent.IsNull()) Parent = Other.Parent;\n              Cost = Other.Cost;\n              State = Other.State;\n              Action = Other.Action;\n      }\n};" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGNode_Statics::NewProp_Parent_MetaData[] = {
		{ "Category", "GNode" },
		{ "ModuleRelativePath", "Public/Components/PlannerComponent.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGNode_Statics::NewProp_Parent = { "Parent", nullptr, (EPropertyFlags)0x0010000000020015, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGNode, Parent), Z_Construct_UClass_UGNode_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGNode_Statics::NewProp_Parent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGNode_Statics::NewProp_Parent_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGNode_Statics::NewProp_Cost_MetaData[] = {
		{ "Category", "GNode" },
		{ "ModuleRelativePath", "Public/Components/PlannerComponent.h" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UGNode_Statics::NewProp_Cost = { "Cost", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGNode, Cost), METADATA_PARAMS(Z_Construct_UClass_UGNode_Statics::NewProp_Cost_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGNode_Statics::NewProp_Cost_MetaData)) };
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGNode_Statics::NewProp_State_ValueProp = { "State", nullptr, (EPropertyFlags)0x0000000000000001, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UClass_UGNode_Statics::NewProp_State_Key_KeyProp = { "State_Key", nullptr, (EPropertyFlags)0x0000000000000001, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGNode_Statics::NewProp_State_MetaData[] = {
		{ "Category", "GNode" },
		{ "ModuleRelativePath", "Public/Components/PlannerComponent.h" },
	};
#endif
	const UECodeGen_Private::FMapPropertyParams Z_Construct_UClass_UGNode_Statics::NewProp_State = { "State", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGNode, State), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGNode_Statics::NewProp_State_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGNode_Statics::NewProp_State_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGNode_Statics::NewProp_Action_MetaData[] = {
		{ "Category", "GNode" },
		{ "ModuleRelativePath", "Public/Components/PlannerComponent.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGNode_Statics::NewProp_Action = { "Action", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGNode, Action), Z_Construct_UClass_UGActionBase_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGNode_Statics::NewProp_Action_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGNode_Statics::NewProp_Action_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGNode_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGNode_Statics::NewProp_Parent,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGNode_Statics::NewProp_Cost,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGNode_Statics::NewProp_State_ValueProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGNode_Statics::NewProp_State_Key_KeyProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGNode_Statics::NewProp_State,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGNode_Statics::NewProp_Action,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGNode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGNode>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UGNode_Statics::ClassParams = {
		&UGNode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGNode_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGNode_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGNode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGNode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGNode()
	{
		if (!Z_Registration_Info_UClass_UGNode.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UGNode.OuterSingleton, Z_Construct_UClass_UGNode_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UGNode.OuterSingleton;
	}
	template<> AIRUNTIME_API UClass* StaticClass<UGNode>()
	{
		return UGNode::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGNode);
	struct Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UPlannerComponent, UPlannerComponent::StaticClass, TEXT("UPlannerComponent"), &Z_Registration_Info_UClass_UPlannerComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UPlannerComponent), 341006548U) },
		{ Z_Construct_UClass_UGNode, UGNode::StaticClass, TEXT("UGNode"), &Z_Registration_Info_UClass_UGNode, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UGNode), 3631583911U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_1215664892(TEXT("/Script/AIRuntime"),
		Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
