// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UAgentData;
#ifdef AIRUNTIME_ActionsComponent_generated_h
#error "ActionsComponent.generated.h already included, missing '#pragma once' in ActionsComponent.h"
#endif
#define AIRUNTIME_ActionsComponent_generated_h

#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_ActionsComponent_h_15_SPARSE_DATA
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_ActionsComponent_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execResetActions); \
	DECLARE_FUNCTION(execInitializeActions);


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_ActionsComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execResetActions); \
	DECLARE_FUNCTION(execInitializeActions);


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_ActionsComponent_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUActionsComponent(); \
	friend struct Z_Construct_UClass_UActionsComponent_Statics; \
public: \
	DECLARE_CLASS(UActionsComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AIRuntime"), NO_API) \
	DECLARE_SERIALIZER(UActionsComponent)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_ActionsComponent_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUActionsComponent(); \
	friend struct Z_Construct_UClass_UActionsComponent_Statics; \
public: \
	DECLARE_CLASS(UActionsComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AIRuntime"), NO_API) \
	DECLARE_SERIALIZER(UActionsComponent)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_ActionsComponent_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UActionsComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UActionsComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UActionsComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActionsComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UActionsComponent(UActionsComponent&&); \
	NO_API UActionsComponent(const UActionsComponent&); \
public:


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_ActionsComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UActionsComponent(UActionsComponent&&); \
	NO_API UActionsComponent(const UActionsComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UActionsComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActionsComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UActionsComponent)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_ActionsComponent_h_12_PROLOG
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_ActionsComponent_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_ActionsComponent_h_15_SPARSE_DATA \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_ActionsComponent_h_15_RPC_WRAPPERS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_ActionsComponent_h_15_INCLASS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_ActionsComponent_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_ActionsComponent_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_ActionsComponent_h_15_SPARSE_DATA \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_ActionsComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_ActionsComponent_h_15_INCLASS_NO_PURE_DECLS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_ActionsComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AIRUNTIME_API UClass* StaticClass<class UActionsComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_ActionsComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
