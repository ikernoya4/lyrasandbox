// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AIRuntime/Actions/FindCoverAction.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFindCoverAction() {}
// Cross Module References
	AIRUNTIME_API UClass* Z_Construct_UClass_UFindCoverAction_NoRegister();
	AIRUNTIME_API UClass* Z_Construct_UClass_UFindCoverAction();
	AIRUNTIME_API UClass* Z_Construct_UClass_UGActionBase();
	UPackage* Z_Construct_UPackage__Script_AIRuntime();
	AIMODULE_API UScriptStruct* Z_Construct_UScriptStruct_FAIRequestID();
	AIMODULE_API UEnum* Z_Construct_UEnum_AIModule_EPathFollowingResult();
	LYRAGAME_API UClass* Z_Construct_UClass_ASandboxCover_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	AIMODULE_API UClass* Z_Construct_UClass_UEnvQuery_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UFindCoverAction::execOnDestinationReached)
	{
		P_GET_STRUCT(FAIRequestID,Z_Param_ID);
		P_GET_PROPERTY(FByteProperty,Z_Param_Result);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnDestinationReached(Z_Param_ID,EPathFollowingResult::Type(Z_Param_Result));
		P_NATIVE_END;
	}
	void UFindCoverAction::StaticRegisterNativesUFindCoverAction()
	{
		UClass* Class = UFindCoverAction::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnDestinationReached", &UFindCoverAction::execOnDestinationReached },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UFindCoverAction_OnDestinationReached_Statics
	{
		struct FindCoverAction_eventOnDestinationReached_Parms
		{
			FAIRequestID ID;
			TEnumAsByte<EPathFollowingResult::Type> Result;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_ID;
		static const UECodeGen_Private::FBytePropertyParams NewProp_Result;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UFindCoverAction_OnDestinationReached_Statics::NewProp_ID = { "ID", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FindCoverAction_eventOnDestinationReached_Parms, ID), Z_Construct_UScriptStruct_FAIRequestID, METADATA_PARAMS(nullptr, 0) }; // 2930851505
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UFindCoverAction_OnDestinationReached_Statics::NewProp_Result = { "Result", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FindCoverAction_eventOnDestinationReached_Parms, Result), Z_Construct_UEnum_AIModule_EPathFollowingResult, METADATA_PARAMS(nullptr, 0) }; // 1916887335
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UFindCoverAction_OnDestinationReached_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UFindCoverAction_OnDestinationReached_Statics::NewProp_ID,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UFindCoverAction_OnDestinationReached_Statics::NewProp_Result,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UFindCoverAction_OnDestinationReached_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Actions/FindCoverAction.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UFindCoverAction_OnDestinationReached_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UFindCoverAction, nullptr, "OnDestinationReached", nullptr, nullptr, sizeof(Z_Construct_UFunction_UFindCoverAction_OnDestinationReached_Statics::FindCoverAction_eventOnDestinationReached_Parms), Z_Construct_UFunction_UFindCoverAction_OnDestinationReached_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UFindCoverAction_OnDestinationReached_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UFindCoverAction_OnDestinationReached_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UFindCoverAction_OnDestinationReached_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UFindCoverAction_OnDestinationReached()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UFindCoverAction_OnDestinationReached_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UFindCoverAction);
	UClass* Z_Construct_UClass_UFindCoverAction_NoRegister()
	{
		return UFindCoverAction::StaticClass();
	}
	struct Z_Construct_UClass_UFindCoverAction_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_SelectedCover_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_SelectedCover;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_SelectedCoverLocation_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_SelectedCoverLocation;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ClosestDistance_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_ClosestDistance;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_FindCoverQuery_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_FindCoverQuery;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFindCoverAction_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGActionBase,
		(UObject* (*)())Z_Construct_UPackage__Script_AIRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UFindCoverAction_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UFindCoverAction_OnDestinationReached, "OnDestinationReached" }, // 1387269559
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFindCoverAction_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "Actions/FindCoverAction.h" },
		{ "ModuleRelativePath", "Actions/FindCoverAction.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFindCoverAction_Statics::NewProp_SelectedCover_MetaData[] = {
		{ "Category", "FindCoverAction" },
		{ "ModuleRelativePath", "Actions/FindCoverAction.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UFindCoverAction_Statics::NewProp_SelectedCover = { "SelectedCover", nullptr, (EPropertyFlags)0x0020080000020015, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFindCoverAction, SelectedCover), Z_Construct_UClass_ASandboxCover_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UFindCoverAction_Statics::NewProp_SelectedCover_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFindCoverAction_Statics::NewProp_SelectedCover_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFindCoverAction_Statics::NewProp_SelectedCoverLocation_MetaData[] = {
		{ "Category", "FindCoverAction" },
		{ "ModuleRelativePath", "Actions/FindCoverAction.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UFindCoverAction_Statics::NewProp_SelectedCoverLocation = { "SelectedCoverLocation", nullptr, (EPropertyFlags)0x0020080000020015, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFindCoverAction, SelectedCoverLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UFindCoverAction_Statics::NewProp_SelectedCoverLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFindCoverAction_Statics::NewProp_SelectedCoverLocation_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFindCoverAction_Statics::NewProp_ClosestDistance_MetaData[] = {
		{ "Category", "FindCoverAction" },
		{ "ModuleRelativePath", "Actions/FindCoverAction.h" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UFindCoverAction_Statics::NewProp_ClosestDistance = { "ClosestDistance", nullptr, (EPropertyFlags)0x0020080000020015, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFindCoverAction, ClosestDistance), METADATA_PARAMS(Z_Construct_UClass_UFindCoverAction_Statics::NewProp_ClosestDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFindCoverAction_Statics::NewProp_ClosestDistance_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFindCoverAction_Statics::NewProp_FindCoverQuery_MetaData[] = {
		{ "Category", "FindCoverAction" },
		{ "ModuleRelativePath", "Actions/FindCoverAction.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UFindCoverAction_Statics::NewProp_FindCoverQuery = { "FindCoverQuery", nullptr, (EPropertyFlags)0x0020080000000015, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFindCoverAction, FindCoverQuery), Z_Construct_UClass_UEnvQuery_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UFindCoverAction_Statics::NewProp_FindCoverQuery_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFindCoverAction_Statics::NewProp_FindCoverQuery_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFindCoverAction_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFindCoverAction_Statics::NewProp_SelectedCover,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFindCoverAction_Statics::NewProp_SelectedCoverLocation,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFindCoverAction_Statics::NewProp_ClosestDistance,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFindCoverAction_Statics::NewProp_FindCoverQuery,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFindCoverAction_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFindCoverAction>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UFindCoverAction_Statics::ClassParams = {
		&UFindCoverAction::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UFindCoverAction_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UFindCoverAction_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFindCoverAction_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFindCoverAction_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFindCoverAction()
	{
		if (!Z_Registration_Info_UClass_UFindCoverAction.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UFindCoverAction.OuterSingleton, Z_Construct_UClass_UFindCoverAction_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UFindCoverAction.OuterSingleton;
	}
	template<> AIRUNTIME_API UClass* StaticClass<UFindCoverAction>()
	{
		return UFindCoverAction::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFindCoverAction);
	struct Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_FindCoverAction_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_FindCoverAction_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UFindCoverAction, UFindCoverAction::StaticClass, TEXT("UFindCoverAction"), &Z_Registration_Info_UClass_UFindCoverAction, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UFindCoverAction), 909347708U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_FindCoverAction_h_1812107823(TEXT("/Script/AIRuntime"),
		Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_FindCoverAction_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_FindCoverAction_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
