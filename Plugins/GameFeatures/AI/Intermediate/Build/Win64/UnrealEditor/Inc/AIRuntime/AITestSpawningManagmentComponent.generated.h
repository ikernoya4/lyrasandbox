// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AIRUNTIME_AITestSpawningManagmentComponent_generated_h
#error "AITestSpawningManagmentComponent.generated.h already included, missing '#pragma once' in AITestSpawningManagmentComponent.h"
#endif
#define AIRUNTIME_AITestSpawningManagmentComponent_generated_h

#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Private_AITestSpawningManagmentComponent_h_15_SPARSE_DATA
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Private_AITestSpawningManagmentComponent_h_15_RPC_WRAPPERS
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Private_AITestSpawningManagmentComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Private_AITestSpawningManagmentComponent_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAITestSpawningManagmentComponent(); \
	friend struct Z_Construct_UClass_UAITestSpawningManagmentComponent_Statics; \
public: \
	DECLARE_CLASS(UAITestSpawningManagmentComponent, ULyraPlayerSpawningManagerComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AIRuntime"), NO_API) \
	DECLARE_SERIALIZER(UAITestSpawningManagmentComponent)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Private_AITestSpawningManagmentComponent_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUAITestSpawningManagmentComponent(); \
	friend struct Z_Construct_UClass_UAITestSpawningManagmentComponent_Statics; \
public: \
	DECLARE_CLASS(UAITestSpawningManagmentComponent, ULyraPlayerSpawningManagerComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AIRuntime"), NO_API) \
	DECLARE_SERIALIZER(UAITestSpawningManagmentComponent)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Private_AITestSpawningManagmentComponent_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAITestSpawningManagmentComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAITestSpawningManagmentComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAITestSpawningManagmentComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAITestSpawningManagmentComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAITestSpawningManagmentComponent(UAITestSpawningManagmentComponent&&); \
	NO_API UAITestSpawningManagmentComponent(const UAITestSpawningManagmentComponent&); \
public:


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Private_AITestSpawningManagmentComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAITestSpawningManagmentComponent(UAITestSpawningManagmentComponent&&); \
	NO_API UAITestSpawningManagmentComponent(const UAITestSpawningManagmentComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAITestSpawningManagmentComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAITestSpawningManagmentComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAITestSpawningManagmentComponent)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Private_AITestSpawningManagmentComponent_h_12_PROLOG
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Private_AITestSpawningManagmentComponent_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Private_AITestSpawningManagmentComponent_h_15_SPARSE_DATA \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Private_AITestSpawningManagmentComponent_h_15_RPC_WRAPPERS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Private_AITestSpawningManagmentComponent_h_15_INCLASS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Private_AITestSpawningManagmentComponent_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Private_AITestSpawningManagmentComponent_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Private_AITestSpawningManagmentComponent_h_15_SPARSE_DATA \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Private_AITestSpawningManagmentComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Private_AITestSpawningManagmentComponent_h_15_INCLASS_NO_PURE_DECLS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Private_AITestSpawningManagmentComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AIRUNTIME_API UClass* StaticClass<class UAITestSpawningManagmentComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Private_AITestSpawningManagmentComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
