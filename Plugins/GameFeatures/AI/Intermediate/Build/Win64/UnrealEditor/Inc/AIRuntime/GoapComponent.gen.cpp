// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AIRuntime/Public/Components/GoapComponent.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGoapComponent() {}
// Cross Module References
	AIRUNTIME_API UClass* Z_Construct_UClass_UGoapComponent_NoRegister();
	AIRUNTIME_API UClass* Z_Construct_UClass_UGoapComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_AIRuntime();
	AIRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FWorldStates();
	AIRUNTIME_API UClass* Z_Construct_UClass_UActionsComponent_NoRegister();
	AIRUNTIME_API UClass* Z_Construct_UClass_UPlannerComponent_NoRegister();
	AIMODULE_API UClass* Z_Construct_UClass_AAIController_NoRegister();
	AIRUNTIME_API UClass* Z_Construct_UClass_UAIWorldState_NoRegister();
	AIRUNTIME_API UClass* Z_Construct_UClass_UAgentData_NoRegister();
	AIRUNTIME_API UClass* Z_Construct_UClass_UGActionBase_NoRegister();
	AIRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FSubGoal();
// End Cross Module References
	DEFINE_FUNCTION(UGoapComponent::execAllowPlanningAfterDelay)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AllowPlanningAfterDelay();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoapComponent::execCompareBeliefsWithWorldState)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_Name);
		P_GET_UBOOL(Z_Param_Value);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CompareBeliefsWithWorldState(Z_Param_Name,Z_Param_Value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoapComponent::execOnActionComplete)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnActionComplete();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoapComponent::execGetBeliefs)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FWorldStates*)Z_Param__Result=P_THIS->GetBeliefs();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoapComponent::execSetBeliefState)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_State);
		P_GET_UBOOL(Z_Param_Value);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetBeliefState(Z_Param_State,Z_Param_Value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoapComponent::execStartCurrentAction)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StartCurrentAction();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoapComponent::execCheckCurrentGoalCompletion)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CheckCurrentGoalCompletion();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoapComponent::execFormulatePlan)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->FormulatePlan();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoapComponent::execReset)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Reset();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoapComponent::execCancelActions)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CancelActions();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoapComponent::execCancelAndReformulatePlan)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CancelAndReformulatePlan();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGoapComponent::execInitialize)
	{
		P_GET_OBJECT(UActionsComponent,Z_Param_ActionsComp);
		P_GET_OBJECT(UPlannerComponent,Z_Param_PlannerComp);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Initialize(Z_Param_ActionsComp,Z_Param_PlannerComp);
		P_NATIVE_END;
	}
	void UGoapComponent::StaticRegisterNativesUGoapComponent()
	{
		UClass* Class = UGoapComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AllowPlanningAfterDelay", &UGoapComponent::execAllowPlanningAfterDelay },
			{ "CancelActions", &UGoapComponent::execCancelActions },
			{ "CancelAndReformulatePlan", &UGoapComponent::execCancelAndReformulatePlan },
			{ "CheckCurrentGoalCompletion", &UGoapComponent::execCheckCurrentGoalCompletion },
			{ "CompareBeliefsWithWorldState", &UGoapComponent::execCompareBeliefsWithWorldState },
			{ "FormulatePlan", &UGoapComponent::execFormulatePlan },
			{ "GetBeliefs", &UGoapComponent::execGetBeliefs },
			{ "Initialize", &UGoapComponent::execInitialize },
			{ "OnActionComplete", &UGoapComponent::execOnActionComplete },
			{ "Reset", &UGoapComponent::execReset },
			{ "SetBeliefState", &UGoapComponent::execSetBeliefState },
			{ "StartCurrentAction", &UGoapComponent::execStartCurrentAction },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UGoapComponent_AllowPlanningAfterDelay_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoapComponent_AllowPlanningAfterDelay_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Components/GoapComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoapComponent_AllowPlanningAfterDelay_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoapComponent, nullptr, "AllowPlanningAfterDelay", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoapComponent_AllowPlanningAfterDelay_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoapComponent_AllowPlanningAfterDelay_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoapComponent_AllowPlanningAfterDelay()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGoapComponent_AllowPlanningAfterDelay_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoapComponent_CancelActions_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoapComponent_CancelActions_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Components/GoapComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoapComponent_CancelActions_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoapComponent, nullptr, "CancelActions", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoapComponent_CancelActions_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoapComponent_CancelActions_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoapComponent_CancelActions()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGoapComponent_CancelActions_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoapComponent_CancelAndReformulatePlan_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoapComponent_CancelAndReformulatePlan_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Components/GoapComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoapComponent_CancelAndReformulatePlan_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoapComponent, nullptr, "CancelAndReformulatePlan", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoapComponent_CancelAndReformulatePlan_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoapComponent_CancelAndReformulatePlan_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoapComponent_CancelAndReformulatePlan()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGoapComponent_CancelAndReformulatePlan_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoapComponent_CheckCurrentGoalCompletion_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoapComponent_CheckCurrentGoalCompletion_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Components/GoapComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoapComponent_CheckCurrentGoalCompletion_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoapComponent, nullptr, "CheckCurrentGoalCompletion", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoapComponent_CheckCurrentGoalCompletion_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoapComponent_CheckCurrentGoalCompletion_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoapComponent_CheckCurrentGoalCompletion()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGoapComponent_CheckCurrentGoalCompletion_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoapComponent_CompareBeliefsWithWorldState_Statics
	{
		struct GoapComponent_eventCompareBeliefsWithWorldState_Parms
		{
			FName Name;
			bool Value;
		};
		static const UECodeGen_Private::FNamePropertyParams NewProp_Name;
		static void NewProp_Value_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_Value;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UGoapComponent_CompareBeliefsWithWorldState_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoapComponent_eventCompareBeliefsWithWorldState_Parms, Name), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UGoapComponent_CompareBeliefsWithWorldState_Statics::NewProp_Value_SetBit(void* Obj)
	{
		((GoapComponent_eventCompareBeliefsWithWorldState_Parms*)Obj)->Value = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoapComponent_CompareBeliefsWithWorldState_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoapComponent_eventCompareBeliefsWithWorldState_Parms), &Z_Construct_UFunction_UGoapComponent_CompareBeliefsWithWorldState_Statics::NewProp_Value_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoapComponent_CompareBeliefsWithWorldState_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoapComponent_CompareBeliefsWithWorldState_Statics::NewProp_Name,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoapComponent_CompareBeliefsWithWorldState_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoapComponent_CompareBeliefsWithWorldState_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Components/GoapComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoapComponent_CompareBeliefsWithWorldState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoapComponent, nullptr, "CompareBeliefsWithWorldState", nullptr, nullptr, sizeof(Z_Construct_UFunction_UGoapComponent_CompareBeliefsWithWorldState_Statics::GoapComponent_eventCompareBeliefsWithWorldState_Parms), Z_Construct_UFunction_UGoapComponent_CompareBeliefsWithWorldState_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoapComponent_CompareBeliefsWithWorldState_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoapComponent_CompareBeliefsWithWorldState_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoapComponent_CompareBeliefsWithWorldState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoapComponent_CompareBeliefsWithWorldState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGoapComponent_CompareBeliefsWithWorldState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoapComponent_FormulatePlan_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoapComponent_FormulatePlan_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Components/GoapComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoapComponent_FormulatePlan_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoapComponent, nullptr, "FormulatePlan", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoapComponent_FormulatePlan_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoapComponent_FormulatePlan_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoapComponent_FormulatePlan()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGoapComponent_FormulatePlan_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoapComponent_GetBeliefs_Statics
	{
		struct GoapComponent_eventGetBeliefs_Parms
		{
			FWorldStates ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGoapComponent_GetBeliefs_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoapComponent_eventGetBeliefs_Parms, ReturnValue), Z_Construct_UScriptStruct_FWorldStates, METADATA_PARAMS(nullptr, 0) }; // 21332049
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoapComponent_GetBeliefs_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoapComponent_GetBeliefs_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoapComponent_GetBeliefs_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Components/GoapComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoapComponent_GetBeliefs_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoapComponent, nullptr, "GetBeliefs", nullptr, nullptr, sizeof(Z_Construct_UFunction_UGoapComponent_GetBeliefs_Statics::GoapComponent_eventGetBeliefs_Parms), Z_Construct_UFunction_UGoapComponent_GetBeliefs_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoapComponent_GetBeliefs_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoapComponent_GetBeliefs_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoapComponent_GetBeliefs_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoapComponent_GetBeliefs()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGoapComponent_GetBeliefs_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoapComponent_Initialize_Statics
	{
		struct GoapComponent_eventInitialize_Parms
		{
			UActionsComponent* ActionsComp;
			UPlannerComponent* PlannerComp;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ActionsComp_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ActionsComp;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_PlannerComp_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_PlannerComp;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoapComponent_Initialize_Statics::NewProp_ActionsComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGoapComponent_Initialize_Statics::NewProp_ActionsComp = { "ActionsComp", nullptr, (EPropertyFlags)0x0010000000080080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoapComponent_eventInitialize_Parms, ActionsComp), Z_Construct_UClass_UActionsComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UGoapComponent_Initialize_Statics::NewProp_ActionsComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoapComponent_Initialize_Statics::NewProp_ActionsComp_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoapComponent_Initialize_Statics::NewProp_PlannerComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGoapComponent_Initialize_Statics::NewProp_PlannerComp = { "PlannerComp", nullptr, (EPropertyFlags)0x0010000000080080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoapComponent_eventInitialize_Parms, PlannerComp), Z_Construct_UClass_UPlannerComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UGoapComponent_Initialize_Statics::NewProp_PlannerComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoapComponent_Initialize_Statics::NewProp_PlannerComp_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoapComponent_Initialize_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoapComponent_Initialize_Statics::NewProp_ActionsComp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoapComponent_Initialize_Statics::NewProp_PlannerComp,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoapComponent_Initialize_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Components/GoapComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoapComponent_Initialize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoapComponent, nullptr, "Initialize", nullptr, nullptr, sizeof(Z_Construct_UFunction_UGoapComponent_Initialize_Statics::GoapComponent_eventInitialize_Parms), Z_Construct_UFunction_UGoapComponent_Initialize_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoapComponent_Initialize_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoapComponent_Initialize_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoapComponent_Initialize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoapComponent_Initialize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGoapComponent_Initialize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoapComponent_OnActionComplete_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoapComponent_OnActionComplete_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Components/GoapComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoapComponent_OnActionComplete_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoapComponent, nullptr, "OnActionComplete", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoapComponent_OnActionComplete_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoapComponent_OnActionComplete_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoapComponent_OnActionComplete()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGoapComponent_OnActionComplete_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoapComponent_Reset_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoapComponent_Reset_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Components/GoapComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoapComponent_Reset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoapComponent, nullptr, "Reset", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoapComponent_Reset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoapComponent_Reset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoapComponent_Reset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGoapComponent_Reset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoapComponent_SetBeliefState_Statics
	{
		struct GoapComponent_eventSetBeliefState_Parms
		{
			FName State;
			bool Value;
		};
		static const UECodeGen_Private::FNamePropertyParams NewProp_State;
		static void NewProp_Value_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_Value;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UGoapComponent_SetBeliefState_Statics::NewProp_State = { "State", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GoapComponent_eventSetBeliefState_Parms, State), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UGoapComponent_SetBeliefState_Statics::NewProp_Value_SetBit(void* Obj)
	{
		((GoapComponent_eventSetBeliefState_Parms*)Obj)->Value = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGoapComponent_SetBeliefState_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GoapComponent_eventSetBeliefState_Parms), &Z_Construct_UFunction_UGoapComponent_SetBeliefState_Statics::NewProp_Value_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGoapComponent_SetBeliefState_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoapComponent_SetBeliefState_Statics::NewProp_State,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGoapComponent_SetBeliefState_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoapComponent_SetBeliefState_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Components/GoapComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoapComponent_SetBeliefState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoapComponent, nullptr, "SetBeliefState", nullptr, nullptr, sizeof(Z_Construct_UFunction_UGoapComponent_SetBeliefState_Statics::GoapComponent_eventSetBeliefState_Parms), Z_Construct_UFunction_UGoapComponent_SetBeliefState_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoapComponent_SetBeliefState_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoapComponent_SetBeliefState_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoapComponent_SetBeliefState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoapComponent_SetBeliefState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGoapComponent_SetBeliefState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGoapComponent_StartCurrentAction_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGoapComponent_StartCurrentAction_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Components/GoapComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGoapComponent_StartCurrentAction_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGoapComponent, nullptr, "StartCurrentAction", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGoapComponent_StartCurrentAction_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGoapComponent_StartCurrentAction_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGoapComponent_StartCurrentAction()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGoapComponent_StartCurrentAction_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UGoapComponent);
	UClass* Z_Construct_UClass_UGoapComponent_NoRegister()
	{
		return UGoapComponent::StaticClass();
	}
	struct Z_Construct_UClass_UGoapComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ActionsComponent_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ActionsComponent;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Planner_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Planner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OwnerController_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_OwnerController;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_WorldState_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldState;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_AgentData_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_AgentData;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_AgentBeliefs_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_AgentBeliefs;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InitialAgentBeliefs_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_InitialAgentBeliefs;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_CurrentAction_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_CurrentAction;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_CurrentGoal_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_CurrentGoal;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ActionQueue_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ActionQueue_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ActionQueue;
		static const UECodeGen_Private::FBoolPropertyParams NewProp_Goals_ValueProp;
		static const UECodeGen_Private::FStructPropertyParams NewProp_Goals_Key_KeyProp;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Goals_MetaData[];
#endif
		static const UECodeGen_Private::FMapPropertyParams NewProp_Goals;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_bIsAgentDead_MetaData[];
#endif
		static void NewProp_bIsAgentDead_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bIsAgentDead;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGoapComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_AIRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UGoapComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UGoapComponent_AllowPlanningAfterDelay, "AllowPlanningAfterDelay" }, // 2408616012
		{ &Z_Construct_UFunction_UGoapComponent_CancelActions, "CancelActions" }, // 2635043306
		{ &Z_Construct_UFunction_UGoapComponent_CancelAndReformulatePlan, "CancelAndReformulatePlan" }, // 3168634457
		{ &Z_Construct_UFunction_UGoapComponent_CheckCurrentGoalCompletion, "CheckCurrentGoalCompletion" }, // 1545963704
		{ &Z_Construct_UFunction_UGoapComponent_CompareBeliefsWithWorldState, "CompareBeliefsWithWorldState" }, // 856777378
		{ &Z_Construct_UFunction_UGoapComponent_FormulatePlan, "FormulatePlan" }, // 647123697
		{ &Z_Construct_UFunction_UGoapComponent_GetBeliefs, "GetBeliefs" }, // 1258230751
		{ &Z_Construct_UFunction_UGoapComponent_Initialize, "Initialize" }, // 1417047425
		{ &Z_Construct_UFunction_UGoapComponent_OnActionComplete, "OnActionComplete" }, // 754312258
		{ &Z_Construct_UFunction_UGoapComponent_Reset, "Reset" }, // 57956201
		{ &Z_Construct_UFunction_UGoapComponent_SetBeliefState, "SetBeliefState" }, // 1606316817
		{ &Z_Construct_UFunction_UGoapComponent_StartCurrentAction, "StartCurrentAction" }, // 3727059267
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoapComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "Components/GoapComponent.h" },
		{ "ModuleRelativePath", "Public/Components/GoapComponent.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoapComponent_Statics::NewProp_ActionsComponent_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Components/GoapComponent.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGoapComponent_Statics::NewProp_ActionsComponent = { "ActionsComponent", nullptr, (EPropertyFlags)0x0040000000080008, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoapComponent, ActionsComponent), Z_Construct_UClass_UActionsComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGoapComponent_Statics::NewProp_ActionsComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoapComponent_Statics::NewProp_ActionsComponent_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoapComponent_Statics::NewProp_Planner_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Components/GoapComponent.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGoapComponent_Statics::NewProp_Planner = { "Planner", nullptr, (EPropertyFlags)0x0040000000080008, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoapComponent, Planner), Z_Construct_UClass_UPlannerComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGoapComponent_Statics::NewProp_Planner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoapComponent_Statics::NewProp_Planner_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoapComponent_Statics::NewProp_OwnerController_MetaData[] = {
		{ "ModuleRelativePath", "Public/Components/GoapComponent.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGoapComponent_Statics::NewProp_OwnerController = { "OwnerController", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoapComponent, OwnerController), Z_Construct_UClass_AAIController_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGoapComponent_Statics::NewProp_OwnerController_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoapComponent_Statics::NewProp_OwnerController_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoapComponent_Statics::NewProp_WorldState_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Components/GoapComponent.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGoapComponent_Statics::NewProp_WorldState = { "WorldState", nullptr, (EPropertyFlags)0x0040000000080008, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoapComponent, WorldState), Z_Construct_UClass_UAIWorldState_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGoapComponent_Statics::NewProp_WorldState_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoapComponent_Statics::NewProp_WorldState_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoapComponent_Statics::NewProp_AgentData_MetaData[] = {
		{ "Category", "AI" },
		{ "ModuleRelativePath", "Public/Components/GoapComponent.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGoapComponent_Statics::NewProp_AgentData = { "AgentData", nullptr, (EPropertyFlags)0x0020080000000005, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoapComponent, AgentData), Z_Construct_UClass_UAgentData_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGoapComponent_Statics::NewProp_AgentData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoapComponent_Statics::NewProp_AgentData_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoapComponent_Statics::NewProp_AgentBeliefs_MetaData[] = {
		{ "Category", "AI" },
		{ "ModuleRelativePath", "Public/Components/GoapComponent.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGoapComponent_Statics::NewProp_AgentBeliefs = { "AgentBeliefs", nullptr, (EPropertyFlags)0x0020080000000005, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoapComponent, AgentBeliefs), Z_Construct_UScriptStruct_FWorldStates, METADATA_PARAMS(Z_Construct_UClass_UGoapComponent_Statics::NewProp_AgentBeliefs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoapComponent_Statics::NewProp_AgentBeliefs_MetaData)) }; // 21332049
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoapComponent_Statics::NewProp_InitialAgentBeliefs_MetaData[] = {
		{ "Category", "AI" },
		{ "ModuleRelativePath", "Public/Components/GoapComponent.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGoapComponent_Statics::NewProp_InitialAgentBeliefs = { "InitialAgentBeliefs", nullptr, (EPropertyFlags)0x0020080000020005, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoapComponent, InitialAgentBeliefs), Z_Construct_UScriptStruct_FWorldStates, METADATA_PARAMS(Z_Construct_UClass_UGoapComponent_Statics::NewProp_InitialAgentBeliefs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoapComponent_Statics::NewProp_InitialAgentBeliefs_MetaData)) }; // 21332049
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoapComponent_Statics::NewProp_CurrentAction_MetaData[] = {
		{ "Category", "AI|Settings" },
		{ "ModuleRelativePath", "Public/Components/GoapComponent.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGoapComponent_Statics::NewProp_CurrentAction = { "CurrentAction", nullptr, (EPropertyFlags)0x0020080000000005, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoapComponent, CurrentAction), Z_Construct_UClass_UGActionBase_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGoapComponent_Statics::NewProp_CurrentAction_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoapComponent_Statics::NewProp_CurrentAction_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoapComponent_Statics::NewProp_CurrentGoal_MetaData[] = {
		{ "Category", "AI|Settings" },
		{ "ModuleRelativePath", "Public/Components/GoapComponent.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGoapComponent_Statics::NewProp_CurrentGoal = { "CurrentGoal", nullptr, (EPropertyFlags)0x0020080000000005, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoapComponent, CurrentGoal), Z_Construct_UScriptStruct_FSubGoal, METADATA_PARAMS(Z_Construct_UClass_UGoapComponent_Statics::NewProp_CurrentGoal_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoapComponent_Statics::NewProp_CurrentGoal_MetaData)) }; // 1865115705
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGoapComponent_Statics::NewProp_ActionQueue_Inner = { "ActionQueue", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UGActionBase_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoapComponent_Statics::NewProp_ActionQueue_MetaData[] = {
		{ "Category", "AI|Settings" },
		{ "ModuleRelativePath", "Public/Components/GoapComponent.h" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGoapComponent_Statics::NewProp_ActionQueue = { "ActionQueue", nullptr, (EPropertyFlags)0x0020080000000005, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoapComponent, ActionQueue), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGoapComponent_Statics::NewProp_ActionQueue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoapComponent_Statics::NewProp_ActionQueue_MetaData)) };
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGoapComponent_Statics::NewProp_Goals_ValueProp = { "Goals", nullptr, (EPropertyFlags)0x0000000000000001, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGoapComponent_Statics::NewProp_Goals_Key_KeyProp = { "Goals_Key", nullptr, (EPropertyFlags)0x0000000000000001, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSubGoal, METADATA_PARAMS(nullptr, 0) }; // 1865115705
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoapComponent_Statics::NewProp_Goals_MetaData[] = {
		{ "Category", "AI|Settings" },
		{ "ModuleRelativePath", "Public/Components/GoapComponent.h" },
	};
#endif
	const UECodeGen_Private::FMapPropertyParams Z_Construct_UClass_UGoapComponent_Statics::NewProp_Goals = { "Goals", nullptr, (EPropertyFlags)0x0020080000000005, UECodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGoapComponent, Goals), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGoapComponent_Statics::NewProp_Goals_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoapComponent_Statics::NewProp_Goals_MetaData)) }; // 1865115705
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGoapComponent_Statics::NewProp_bIsAgentDead_MetaData[] = {
		{ "Category", "AI" },
		{ "ModuleRelativePath", "Public/Components/GoapComponent.h" },
	};
#endif
	void Z_Construct_UClass_UGoapComponent_Statics::NewProp_bIsAgentDead_SetBit(void* Obj)
	{
		((UGoapComponent*)Obj)->bIsAgentDead = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGoapComponent_Statics::NewProp_bIsAgentDead = { "bIsAgentDead", nullptr, (EPropertyFlags)0x0020080000020005, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGoapComponent), &Z_Construct_UClass_UGoapComponent_Statics::NewProp_bIsAgentDead_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGoapComponent_Statics::NewProp_bIsAgentDead_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGoapComponent_Statics::NewProp_bIsAgentDead_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGoapComponent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoapComponent_Statics::NewProp_ActionsComponent,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoapComponent_Statics::NewProp_Planner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoapComponent_Statics::NewProp_OwnerController,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoapComponent_Statics::NewProp_WorldState,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoapComponent_Statics::NewProp_AgentData,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoapComponent_Statics::NewProp_AgentBeliefs,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoapComponent_Statics::NewProp_InitialAgentBeliefs,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoapComponent_Statics::NewProp_CurrentAction,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoapComponent_Statics::NewProp_CurrentGoal,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoapComponent_Statics::NewProp_ActionQueue_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoapComponent_Statics::NewProp_ActionQueue,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoapComponent_Statics::NewProp_Goals_ValueProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoapComponent_Statics::NewProp_Goals_Key_KeyProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoapComponent_Statics::NewProp_Goals,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGoapComponent_Statics::NewProp_bIsAgentDead,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGoapComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGoapComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UGoapComponent_Statics::ClassParams = {
		&UGoapComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UGoapComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UGoapComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UGoapComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGoapComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGoapComponent()
	{
		if (!Z_Registration_Info_UClass_UGoapComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UGoapComponent.OuterSingleton, Z_Construct_UClass_UGoapComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UGoapComponent.OuterSingleton;
	}
	template<> AIRUNTIME_API UClass* StaticClass<UGoapComponent>()
	{
		return UGoapComponent::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGoapComponent);
	struct Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_GoapComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_GoapComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UGoapComponent, UGoapComponent::StaticClass, TEXT("UGoapComponent"), &Z_Registration_Info_UClass_UGoapComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UGoapComponent), 254531137U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_GoapComponent_h_735677400(TEXT("/Script/AIRuntime"),
		Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_GoapComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_GoapComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
