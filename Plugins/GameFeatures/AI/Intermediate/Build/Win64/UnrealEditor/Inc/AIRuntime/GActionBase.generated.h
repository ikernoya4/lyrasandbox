// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AIRUNTIME_GActionBase_generated_h
#error "GActionBase.generated.h already included, missing '#pragma once' in GActionBase.h"
#endif
#define AIRUNTIME_GActionBase_generated_h

#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_15_DELEGATE \
static inline void FOnComplete_DelegateWrapper(const FMulticastScriptDelegate& OnComplete) \
{ \
	OnComplete.ProcessMulticastDelegate<UObject>(NULL); \
}


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_20_SPARSE_DATA
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_20_RPC_WRAPPERS \
	virtual void TickAction_Implementation(float DeltaTime); \
	virtual bool PostPerform_Implementation(bool bWasCanceled); \
	virtual void Act_Implementation(); \
	virtual bool PrePerform_Implementation(); \
 \
	DECLARE_FUNCTION(execTickAction); \
	DECLARE_FUNCTION(execCancelAction); \
	DECLARE_FUNCTION(execPostPerform); \
	DECLARE_FUNCTION(execAct); \
	DECLARE_FUNCTION(execPrePerform);


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void TickAction_Implementation(float DeltaTime); \
	virtual bool PostPerform_Implementation(bool bWasCanceled); \
	virtual void Act_Implementation(); \
	virtual bool PrePerform_Implementation(); \
 \
	DECLARE_FUNCTION(execTickAction); \
	DECLARE_FUNCTION(execCancelAction); \
	DECLARE_FUNCTION(execPostPerform); \
	DECLARE_FUNCTION(execAct); \
	DECLARE_FUNCTION(execPrePerform);


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_20_EVENT_PARMS \
	struct GActionBase_eventPostPerform_Parms \
	{ \
		bool bWasCanceled; \
		bool ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		GActionBase_eventPostPerform_Parms() \
			: ReturnValue(false) \
		{ \
		} \
	}; \
	struct GActionBase_eventPrePerform_Parms \
	{ \
		bool ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		GActionBase_eventPrePerform_Parms() \
			: ReturnValue(false) \
		{ \
		} \
	}; \
	struct GActionBase_eventTickAction_Parms \
	{ \
		float DeltaTime; \
	};


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_20_CALLBACK_WRAPPERS
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGActionBase(); \
	friend struct Z_Construct_UClass_UGActionBase_Statics; \
public: \
	DECLARE_CLASS(UGActionBase, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AIRuntime"), NO_API) \
	DECLARE_SERIALIZER(UGActionBase)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUGActionBase(); \
	friend struct Z_Construct_UClass_UGActionBase_Statics; \
public: \
	DECLARE_CLASS(UGActionBase, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AIRuntime"), NO_API) \
	DECLARE_SERIALIZER(UGActionBase)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGActionBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGActionBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGActionBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGActionBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGActionBase(UGActionBase&&); \
	NO_API UGActionBase(const UGActionBase&); \
public:


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGActionBase(UGActionBase&&); \
	NO_API UGActionBase(const UGActionBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGActionBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGActionBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGActionBase)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_17_PROLOG \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_20_EVENT_PARMS


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_20_SPARSE_DATA \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_20_RPC_WRAPPERS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_20_CALLBACK_WRAPPERS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_20_INCLASS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_20_SPARSE_DATA \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_20_CALLBACK_WRAPPERS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_20_INCLASS_NO_PURE_DECLS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AIRUNTIME_API UClass* StaticClass<class UGActionBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
