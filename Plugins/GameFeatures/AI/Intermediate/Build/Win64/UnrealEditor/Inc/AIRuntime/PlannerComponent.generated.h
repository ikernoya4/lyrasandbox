// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UGActionBase;
 
struct FWorldStates;
#ifdef AIRUNTIME_PlannerComponent_generated_h
#error "PlannerComponent.generated.h already included, missing '#pragma once' in PlannerComponent.h"
#endif
#define AIRUNTIME_PlannerComponent_generated_h

#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_18_SPARSE_DATA
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execPlan);


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execPlan);


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPlannerComponent(); \
	friend struct Z_Construct_UClass_UPlannerComponent_Statics; \
public: \
	DECLARE_CLASS(UPlannerComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AIRuntime"), NO_API) \
	DECLARE_SERIALIZER(UPlannerComponent)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUPlannerComponent(); \
	friend struct Z_Construct_UClass_UPlannerComponent_Statics; \
public: \
	DECLARE_CLASS(UPlannerComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AIRuntime"), NO_API) \
	DECLARE_SERIALIZER(UPlannerComponent)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlannerComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlannerComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlannerComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlannerComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlannerComponent(UPlannerComponent&&); \
	NO_API UPlannerComponent(const UPlannerComponent&); \
public:


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlannerComponent(UPlannerComponent&&); \
	NO_API UPlannerComponent(const UPlannerComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlannerComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlannerComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPlannerComponent)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_15_PROLOG
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_18_SPARSE_DATA \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_18_RPC_WRAPPERS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_18_INCLASS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_18_SPARSE_DATA \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_18_INCLASS_NO_PURE_DECLS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AIRUNTIME_API UClass* StaticClass<class UPlannerComponent>();

#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_68_SPARSE_DATA
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_68_RPC_WRAPPERS
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_68_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_68_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGNode(); \
	friend struct Z_Construct_UClass_UGNode_Statics; \
public: \
	DECLARE_CLASS(UGNode, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AIRuntime"), NO_API) \
	DECLARE_SERIALIZER(UGNode)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_68_INCLASS \
private: \
	static void StaticRegisterNativesUGNode(); \
	friend struct Z_Construct_UClass_UGNode_Statics; \
public: \
	DECLARE_CLASS(UGNode, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AIRuntime"), NO_API) \
	DECLARE_SERIALIZER(UGNode)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_68_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGNode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGNode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGNode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGNode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGNode(UGNode&&); \
	NO_API UGNode(const UGNode&); \
public:


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_68_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGNode(UGNode&&); \
	NO_API UGNode(const UGNode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGNode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGNode); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGNode)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_65_PROLOG
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_68_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_68_SPARSE_DATA \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_68_RPC_WRAPPERS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_68_INCLASS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_68_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_68_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_68_SPARSE_DATA \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_68_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_68_INCLASS_NO_PURE_DECLS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h_68_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AIRUNTIME_API UClass* StaticClass<class UGNode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_PlannerComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
