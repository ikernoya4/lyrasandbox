// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FWorldStates;
class UActionsComponent;
class UPlannerComponent;
#ifdef AIRUNTIME_GoapComponent_generated_h
#error "GoapComponent.generated.h already included, missing '#pragma once' in GoapComponent.h"
#endif
#define AIRUNTIME_GoapComponent_generated_h

#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_GoapComponent_h_16_SPARSE_DATA
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_GoapComponent_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execAllowPlanningAfterDelay); \
	DECLARE_FUNCTION(execCompareBeliefsWithWorldState); \
	DECLARE_FUNCTION(execOnActionComplete); \
	DECLARE_FUNCTION(execGetBeliefs); \
	DECLARE_FUNCTION(execSetBeliefState); \
	DECLARE_FUNCTION(execStartCurrentAction); \
	DECLARE_FUNCTION(execCheckCurrentGoalCompletion); \
	DECLARE_FUNCTION(execFormulatePlan); \
	DECLARE_FUNCTION(execReset); \
	DECLARE_FUNCTION(execCancelActions); \
	DECLARE_FUNCTION(execCancelAndReformulatePlan); \
	DECLARE_FUNCTION(execInitialize);


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_GoapComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execAllowPlanningAfterDelay); \
	DECLARE_FUNCTION(execCompareBeliefsWithWorldState); \
	DECLARE_FUNCTION(execOnActionComplete); \
	DECLARE_FUNCTION(execGetBeliefs); \
	DECLARE_FUNCTION(execSetBeliefState); \
	DECLARE_FUNCTION(execStartCurrentAction); \
	DECLARE_FUNCTION(execCheckCurrentGoalCompletion); \
	DECLARE_FUNCTION(execFormulatePlan); \
	DECLARE_FUNCTION(execReset); \
	DECLARE_FUNCTION(execCancelActions); \
	DECLARE_FUNCTION(execCancelAndReformulatePlan); \
	DECLARE_FUNCTION(execInitialize);


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_GoapComponent_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGoapComponent(); \
	friend struct Z_Construct_UClass_UGoapComponent_Statics; \
public: \
	DECLARE_CLASS(UGoapComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AIRuntime"), NO_API) \
	DECLARE_SERIALIZER(UGoapComponent)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_GoapComponent_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUGoapComponent(); \
	friend struct Z_Construct_UClass_UGoapComponent_Statics; \
public: \
	DECLARE_CLASS(UGoapComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AIRuntime"), NO_API) \
	DECLARE_SERIALIZER(UGoapComponent)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_GoapComponent_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGoapComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGoapComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoapComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoapComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoapComponent(UGoapComponent&&); \
	NO_API UGoapComponent(const UGoapComponent&); \
public:


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_GoapComponent_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGoapComponent(UGoapComponent&&); \
	NO_API UGoapComponent(const UGoapComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGoapComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGoapComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGoapComponent)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_GoapComponent_h_13_PROLOG
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_GoapComponent_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_GoapComponent_h_16_SPARSE_DATA \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_GoapComponent_h_16_RPC_WRAPPERS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_GoapComponent_h_16_INCLASS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_GoapComponent_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_GoapComponent_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_GoapComponent_h_16_SPARSE_DATA \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_GoapComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_GoapComponent_h_16_INCLASS_NO_PURE_DECLS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_GoapComponent_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AIRUNTIME_API UClass* StaticClass<class UGoapComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_GoapComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
