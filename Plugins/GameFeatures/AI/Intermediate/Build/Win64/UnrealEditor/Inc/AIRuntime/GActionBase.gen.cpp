// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AIRuntime/Actions/GActionBase.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGActionBase() {}
// Cross Module References
	AIRUNTIME_API UFunction* Z_Construct_UDelegateFunction_AIRuntime_OnComplete__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_AIRuntime();
	AIRUNTIME_API UClass* Z_Construct_UClass_UGActionBase_NoRegister();
	AIRUNTIME_API UClass* Z_Construct_UClass_UGActionBase();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	AIMODULE_API UClass* Z_Construct_UClass_AAIController_NoRegister();
	GAMEPLAYTAGS_API UScriptStruct* Z_Construct_UScriptStruct_FGameplayTag();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	AIRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FWorldState();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_AIRuntime_OnComplete__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_AIRuntime_OnComplete__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "ModuleRelativePath", "Actions/GActionBase.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_AIRuntime_OnComplete__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_AIRuntime, nullptr, "OnComplete__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_AIRuntime_OnComplete__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_AIRuntime_OnComplete__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_AIRuntime_OnComplete__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UDelegateFunction_AIRuntime_OnComplete__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UGActionBase::execTickAction)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_DeltaTime);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->TickAction_Implementation(Z_Param_DeltaTime);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGActionBase::execCancelAction)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CancelAction();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGActionBase::execPostPerform)
	{
		P_GET_UBOOL(Z_Param_bWasCanceled);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->PostPerform_Implementation(Z_Param_bWasCanceled);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGActionBase::execAct)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Act_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGActionBase::execPrePerform)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->PrePerform_Implementation();
		P_NATIVE_END;
	}
	static FName NAME_UGActionBase_Act = FName(TEXT("Act"));
	void UGActionBase::Act()
	{
		ProcessEvent(FindFunctionChecked(NAME_UGActionBase_Act),NULL);
	}
	static FName NAME_UGActionBase_PostPerform = FName(TEXT("PostPerform"));
	bool UGActionBase::PostPerform(bool bWasCanceled)
	{
		GActionBase_eventPostPerform_Parms Parms;
		Parms.bWasCanceled=bWasCanceled ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_UGActionBase_PostPerform),&Parms);
		return !!Parms.ReturnValue;
	}
	static FName NAME_UGActionBase_PrePerform = FName(TEXT("PrePerform"));
	bool UGActionBase::PrePerform()
	{
		GActionBase_eventPrePerform_Parms Parms;
		ProcessEvent(FindFunctionChecked(NAME_UGActionBase_PrePerform),&Parms);
		return !!Parms.ReturnValue;
	}
	static FName NAME_UGActionBase_TickAction = FName(TEXT("TickAction"));
	void UGActionBase::TickAction(float DeltaTime)
	{
		GActionBase_eventTickAction_Parms Parms;
		Parms.DeltaTime=DeltaTime;
		ProcessEvent(FindFunctionChecked(NAME_UGActionBase_TickAction),&Parms);
	}
	void UGActionBase::StaticRegisterNativesUGActionBase()
	{
		UClass* Class = UGActionBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Act", &UGActionBase::execAct },
			{ "CancelAction", &UGActionBase::execCancelAction },
			{ "PostPerform", &UGActionBase::execPostPerform },
			{ "PrePerform", &UGActionBase::execPrePerform },
			{ "TickAction", &UGActionBase::execTickAction },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UGActionBase_Act_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGActionBase_Act_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Actions/GActionBase.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGActionBase_Act_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGActionBase, nullptr, "Act", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGActionBase_Act_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGActionBase_Act_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGActionBase_Act()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGActionBase_Act_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGActionBase_CancelAction_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGActionBase_CancelAction_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Actions/GActionBase.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGActionBase_CancelAction_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGActionBase, nullptr, "CancelAction", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGActionBase_CancelAction_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGActionBase_CancelAction_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGActionBase_CancelAction()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGActionBase_CancelAction_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGActionBase_PostPerform_Statics
	{
		static void NewProp_bWasCanceled_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bWasCanceled;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGActionBase_PostPerform_Statics::NewProp_bWasCanceled_SetBit(void* Obj)
	{
		((GActionBase_eventPostPerform_Parms*)Obj)->bWasCanceled = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGActionBase_PostPerform_Statics::NewProp_bWasCanceled = { "bWasCanceled", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GActionBase_eventPostPerform_Parms), &Z_Construct_UFunction_UGActionBase_PostPerform_Statics::NewProp_bWasCanceled_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UGActionBase_PostPerform_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((GActionBase_eventPostPerform_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGActionBase_PostPerform_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GActionBase_eventPostPerform_Parms), &Z_Construct_UFunction_UGActionBase_PostPerform_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGActionBase_PostPerform_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGActionBase_PostPerform_Statics::NewProp_bWasCanceled,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGActionBase_PostPerform_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGActionBase_PostPerform_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Actions/GActionBase.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGActionBase_PostPerform_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGActionBase, nullptr, "PostPerform", nullptr, nullptr, sizeof(GActionBase_eventPostPerform_Parms), Z_Construct_UFunction_UGActionBase_PostPerform_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGActionBase_PostPerform_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGActionBase_PostPerform_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGActionBase_PostPerform_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGActionBase_PostPerform()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGActionBase_PostPerform_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGActionBase_PrePerform_Statics
	{
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UGActionBase_PrePerform_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((GActionBase_eventPrePerform_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGActionBase_PrePerform_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GActionBase_eventPrePerform_Parms), &Z_Construct_UFunction_UGActionBase_PrePerform_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGActionBase_PrePerform_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGActionBase_PrePerform_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGActionBase_PrePerform_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Actions/GActionBase.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGActionBase_PrePerform_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGActionBase, nullptr, "PrePerform", nullptr, nullptr, sizeof(GActionBase_eventPrePerform_Parms), Z_Construct_UFunction_UGActionBase_PrePerform_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGActionBase_PrePerform_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGActionBase_PrePerform_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGActionBase_PrePerform_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGActionBase_PrePerform()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGActionBase_PrePerform_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGActionBase_TickAction_Statics
	{
		static const UECodeGen_Private::FFloatPropertyParams NewProp_DeltaTime;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGActionBase_TickAction_Statics::NewProp_DeltaTime = { "DeltaTime", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GActionBase_eventTickAction_Parms, DeltaTime), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGActionBase_TickAction_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGActionBase_TickAction_Statics::NewProp_DeltaTime,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGActionBase_TickAction_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Actions/GActionBase.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGActionBase_TickAction_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGActionBase, nullptr, "TickAction", nullptr, nullptr, sizeof(GActionBase_eventTickAction_Parms), Z_Construct_UFunction_UGActionBase_TickAction_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGActionBase_TickAction_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGActionBase_TickAction_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGActionBase_TickAction_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGActionBase_TickAction()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGActionBase_TickAction_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UGActionBase);
	UClass* Z_Construct_UClass_UGActionBase_NoRegister()
	{
		return UGActionBase::StaticClass();
	}
	struct Z_Construct_UClass_UGActionBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UECodeGen_Private::FBoolPropertyParams NewProp_Preconditions_Map_ValueProp;
		static const UECodeGen_Private::FNamePropertyParams NewProp_Preconditions_Map_Key_KeyProp;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Preconditions_Map_MetaData[];
#endif
		static const UECodeGen_Private::FMapPropertyParams NewProp_Preconditions_Map;
		static const UECodeGen_Private::FBoolPropertyParams NewProp_Effects_Map_ValueProp;
		static const UECodeGen_Private::FNamePropertyParams NewProp_Effects_Map_Key_KeyProp;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Effects_Map_MetaData[];
#endif
		static const UECodeGen_Private::FMapPropertyParams NewProp_Effects_Map;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Controller_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Controller;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ActionName_MetaData[];
#endif
		static const UECodeGen_Private::FNamePropertyParams NewProp_ActionName;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ActionTag_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_ActionTag;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Cost_MetaData[];
#endif
		static const UECodeGen_Private::FIntPropertyParams NewProp_Cost;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Target_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Target;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_TargetClass_MetaData[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_TargetClass;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Duration_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_Duration;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_bIsInProgress_MetaData[];
#endif
		static void NewProp_bIsInProgress_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bIsInProgress;
		static const UECodeGen_Private::FStructPropertyParams NewProp_Preconditions_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Preconditions_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_Preconditions;
		static const UECodeGen_Private::FStructPropertyParams NewProp_Effects_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Effects_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_Effects;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_bRunning_MetaData[];
#endif
		static void NewProp_bRunning_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bRunning;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_bShouldTick_MetaData[];
#endif
		static void NewProp_bShouldTick_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bShouldTick;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OnComplete_MetaData[];
#endif
		static const UECodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnComplete;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGActionBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_AIRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UGActionBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UGActionBase_Act, "Act" }, // 463775232
		{ &Z_Construct_UFunction_UGActionBase_CancelAction, "CancelAction" }, // 1985213928
		{ &Z_Construct_UFunction_UGActionBase_PostPerform, "PostPerform" }, // 2539389694
		{ &Z_Construct_UFunction_UGActionBase_PrePerform, "PrePerform" }, // 3137955839
		{ &Z_Construct_UFunction_UGActionBase_TickAction, "TickAction" }, // 1909592637
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGActionBase_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Actions/GActionBase.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Actions/GActionBase.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGActionBase_Statics::NewProp_Preconditions_Map_ValueProp = { "Preconditions_Map", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UClass_UGActionBase_Statics::NewProp_Preconditions_Map_Key_KeyProp = { "Preconditions_Map_Key", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGActionBase_Statics::NewProp_Preconditions_Map_MetaData[] = {
		{ "ModuleRelativePath", "Actions/GActionBase.h" },
	};
#endif
	const UECodeGen_Private::FMapPropertyParams Z_Construct_UClass_UGActionBase_Statics::NewProp_Preconditions_Map = { "Preconditions_Map", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGActionBase, Preconditions_Map), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGActionBase_Statics::NewProp_Preconditions_Map_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGActionBase_Statics::NewProp_Preconditions_Map_MetaData)) };
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGActionBase_Statics::NewProp_Effects_Map_ValueProp = { "Effects_Map", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UClass_UGActionBase_Statics::NewProp_Effects_Map_Key_KeyProp = { "Effects_Map_Key", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGActionBase_Statics::NewProp_Effects_Map_MetaData[] = {
		{ "ModuleRelativePath", "Actions/GActionBase.h" },
	};
#endif
	const UECodeGen_Private::FMapPropertyParams Z_Construct_UClass_UGActionBase_Statics::NewProp_Effects_Map = { "Effects_Map", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGActionBase, Effects_Map), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGActionBase_Statics::NewProp_Effects_Map_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGActionBase_Statics::NewProp_Effects_Map_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGActionBase_Statics::NewProp_Controller_MetaData[] = {
		{ "Category", "GActionBase" },
		{ "ModuleRelativePath", "Actions/GActionBase.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGActionBase_Statics::NewProp_Controller = { "Controller", nullptr, (EPropertyFlags)0x0020080000020015, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGActionBase, Controller), Z_Construct_UClass_AAIController_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGActionBase_Statics::NewProp_Controller_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGActionBase_Statics::NewProp_Controller_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGActionBase_Statics::NewProp_ActionName_MetaData[] = {
		{ "Category", "GAction" },
		{ "ModuleRelativePath", "Actions/GActionBase.h" },
	};
#endif
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UClass_UGActionBase_Statics::NewProp_ActionName = { "ActionName", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGActionBase, ActionName), METADATA_PARAMS(Z_Construct_UClass_UGActionBase_Statics::NewProp_ActionName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGActionBase_Statics::NewProp_ActionName_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGActionBase_Statics::NewProp_ActionTag_MetaData[] = {
		{ "Category", "GAction" },
		{ "ModuleRelativePath", "Actions/GActionBase.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGActionBase_Statics::NewProp_ActionTag = { "ActionTag", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGActionBase, ActionTag), Z_Construct_UScriptStruct_FGameplayTag, METADATA_PARAMS(Z_Construct_UClass_UGActionBase_Statics::NewProp_ActionTag_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGActionBase_Statics::NewProp_ActionTag_MetaData)) }; // 802167388
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGActionBase_Statics::NewProp_Cost_MetaData[] = {
		{ "Category", "GAction" },
		{ "ModuleRelativePath", "Actions/GActionBase.h" },
	};
#endif
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UClass_UGActionBase_Statics::NewProp_Cost = { "Cost", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGActionBase, Cost), METADATA_PARAMS(Z_Construct_UClass_UGActionBase_Statics::NewProp_Cost_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGActionBase_Statics::NewProp_Cost_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGActionBase_Statics::NewProp_Target_MetaData[] = {
		{ "Category", "GAction" },
		{ "ModuleRelativePath", "Actions/GActionBase.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGActionBase_Statics::NewProp_Target = { "Target", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGActionBase, Target), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGActionBase_Statics::NewProp_Target_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGActionBase_Statics::NewProp_Target_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGActionBase_Statics::NewProp_TargetClass_MetaData[] = {
		{ "Category", "GAction" },
		{ "ModuleRelativePath", "Actions/GActionBase.h" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UClass_UGActionBase_Statics::NewProp_TargetClass = { "TargetClass", nullptr, (EPropertyFlags)0x0014000000000005, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGActionBase, TargetClass), Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UGActionBase_Statics::NewProp_TargetClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGActionBase_Statics::NewProp_TargetClass_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGActionBase_Statics::NewProp_Duration_MetaData[] = {
		{ "Category", "GAction" },
		{ "ModuleRelativePath", "Actions/GActionBase.h" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UGActionBase_Statics::NewProp_Duration = { "Duration", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGActionBase, Duration), METADATA_PARAMS(Z_Construct_UClass_UGActionBase_Statics::NewProp_Duration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGActionBase_Statics::NewProp_Duration_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGActionBase_Statics::NewProp_bIsInProgress_MetaData[] = {
		{ "Category", "GAction|Settings" },
		{ "ModuleRelativePath", "Actions/GActionBase.h" },
	};
#endif
	void Z_Construct_UClass_UGActionBase_Statics::NewProp_bIsInProgress_SetBit(void* Obj)
	{
		((UGActionBase*)Obj)->bIsInProgress = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGActionBase_Statics::NewProp_bIsInProgress = { "bIsInProgress", nullptr, (EPropertyFlags)0x0010000000020005, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGActionBase), &Z_Construct_UClass_UGActionBase_Statics::NewProp_bIsInProgress_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGActionBase_Statics::NewProp_bIsInProgress_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGActionBase_Statics::NewProp_bIsInProgress_MetaData)) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGActionBase_Statics::NewProp_Preconditions_Inner = { "Preconditions", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FWorldState, METADATA_PARAMS(nullptr, 0) }; // 948963605
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGActionBase_Statics::NewProp_Preconditions_MetaData[] = {
		{ "Category", "GAction|Settings" },
		{ "ModuleRelativePath", "Actions/GActionBase.h" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGActionBase_Statics::NewProp_Preconditions = { "Preconditions", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGActionBase, Preconditions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGActionBase_Statics::NewProp_Preconditions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGActionBase_Statics::NewProp_Preconditions_MetaData)) }; // 948963605
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGActionBase_Statics::NewProp_Effects_Inner = { "Effects", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FWorldState, METADATA_PARAMS(nullptr, 0) }; // 948963605
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGActionBase_Statics::NewProp_Effects_MetaData[] = {
		{ "Category", "GAction|Settings" },
		{ "ModuleRelativePath", "Actions/GActionBase.h" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UGActionBase_Statics::NewProp_Effects = { "Effects", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGActionBase, Effects), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGActionBase_Statics::NewProp_Effects_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGActionBase_Statics::NewProp_Effects_MetaData)) }; // 948963605
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGActionBase_Statics::NewProp_bRunning_MetaData[] = {
		{ "Category", "GAction|Settings" },
		{ "ModuleRelativePath", "Actions/GActionBase.h" },
	};
#endif
	void Z_Construct_UClass_UGActionBase_Statics::NewProp_bRunning_SetBit(void* Obj)
	{
		((UGActionBase*)Obj)->bRunning = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGActionBase_Statics::NewProp_bRunning = { "bRunning", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGActionBase), &Z_Construct_UClass_UGActionBase_Statics::NewProp_bRunning_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGActionBase_Statics::NewProp_bRunning_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGActionBase_Statics::NewProp_bRunning_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGActionBase_Statics::NewProp_bShouldTick_MetaData[] = {
		{ "Category", "GAction|Settings" },
		{ "ModuleRelativePath", "Actions/GActionBase.h" },
	};
#endif
	void Z_Construct_UClass_UGActionBase_Statics::NewProp_bShouldTick_SetBit(void* Obj)
	{
		((UGActionBase*)Obj)->bShouldTick = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGActionBase_Statics::NewProp_bShouldTick = { "bShouldTick", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGActionBase), &Z_Construct_UClass_UGActionBase_Statics::NewProp_bShouldTick_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGActionBase_Statics::NewProp_bShouldTick_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGActionBase_Statics::NewProp_bShouldTick_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGActionBase_Statics::NewProp_OnComplete_MetaData[] = {
		{ "ModuleRelativePath", "Actions/GActionBase.h" },
	};
#endif
	const UECodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UGActionBase_Statics::NewProp_OnComplete = { "OnComplete", nullptr, (EPropertyFlags)0x0010100010080000, UECodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGActionBase, OnComplete), Z_Construct_UDelegateFunction_AIRuntime_OnComplete__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UGActionBase_Statics::NewProp_OnComplete_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGActionBase_Statics::NewProp_OnComplete_MetaData)) }; // 2912576647
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGActionBase_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGActionBase_Statics::NewProp_Preconditions_Map_ValueProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGActionBase_Statics::NewProp_Preconditions_Map_Key_KeyProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGActionBase_Statics::NewProp_Preconditions_Map,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGActionBase_Statics::NewProp_Effects_Map_ValueProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGActionBase_Statics::NewProp_Effects_Map_Key_KeyProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGActionBase_Statics::NewProp_Effects_Map,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGActionBase_Statics::NewProp_Controller,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGActionBase_Statics::NewProp_ActionName,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGActionBase_Statics::NewProp_ActionTag,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGActionBase_Statics::NewProp_Cost,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGActionBase_Statics::NewProp_Target,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGActionBase_Statics::NewProp_TargetClass,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGActionBase_Statics::NewProp_Duration,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGActionBase_Statics::NewProp_bIsInProgress,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGActionBase_Statics::NewProp_Preconditions_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGActionBase_Statics::NewProp_Preconditions,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGActionBase_Statics::NewProp_Effects_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGActionBase_Statics::NewProp_Effects,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGActionBase_Statics::NewProp_bRunning,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGActionBase_Statics::NewProp_bShouldTick,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGActionBase_Statics::NewProp_OnComplete,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGActionBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGActionBase>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UGActionBase_Statics::ClassParams = {
		&UGActionBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UGActionBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UGActionBase_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGActionBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGActionBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGActionBase()
	{
		if (!Z_Registration_Info_UClass_UGActionBase.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UGActionBase.OuterSingleton, Z_Construct_UClass_UGActionBase_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UGActionBase.OuterSingleton;
	}
	template<> AIRUNTIME_API UClass* StaticClass<UGActionBase>()
	{
		return UGActionBase::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGActionBase);
	struct Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UGActionBase, UGActionBase::StaticClass, TEXT("UGActionBase"), &Z_Registration_Info_UClass_UGActionBase, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UGActionBase), 2263737548U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_4178435139(TEXT("/Script/AIRuntime"),
		Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_GActionBase_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
