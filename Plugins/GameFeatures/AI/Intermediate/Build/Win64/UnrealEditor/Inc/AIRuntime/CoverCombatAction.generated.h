// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
#ifdef AIRUNTIME_CoverCombatAction_generated_h
#error "CoverCombatAction.generated.h already included, missing '#pragma once' in CoverCombatAction.h"
#endif
#define AIRUNTIME_CoverCombatAction_generated_h

#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_16_SPARSE_DATA
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetBackToCover); \
	DECLARE_FUNCTION(execMoveOutOfCover);


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetBackToCover); \
	DECLARE_FUNCTION(execMoveOutOfCover);


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_16_EVENT_PARMS \
	struct CoverCombatAction_eventCheckOffsetPosition_Parms \
	{ \
		FVector LeftOffset; \
		FVector RightOffset; \
		FVector HeightOffset; \
		AActor* Enemy; \
		bool ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		CoverCombatAction_eventCheckOffsetPosition_Parms() \
			: ReturnValue(false) \
		{ \
		} \
	};


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_16_CALLBACK_WRAPPERS
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCoverCombatAction(); \
	friend struct Z_Construct_UClass_UCoverCombatAction_Statics; \
public: \
	DECLARE_CLASS(UCoverCombatAction, UGActionBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AIRuntime"), NO_API) \
	DECLARE_SERIALIZER(UCoverCombatAction)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUCoverCombatAction(); \
	friend struct Z_Construct_UClass_UCoverCombatAction_Statics; \
public: \
	DECLARE_CLASS(UCoverCombatAction, UGActionBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AIRuntime"), NO_API) \
	DECLARE_SERIALIZER(UCoverCombatAction)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCoverCombatAction(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCoverCombatAction) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCoverCombatAction); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCoverCombatAction); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCoverCombatAction(UCoverCombatAction&&); \
	NO_API UCoverCombatAction(const UCoverCombatAction&); \
public:


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCoverCombatAction(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCoverCombatAction(UCoverCombatAction&&); \
	NO_API UCoverCombatAction(const UCoverCombatAction&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCoverCombatAction); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCoverCombatAction); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCoverCombatAction)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_13_PROLOG \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_16_EVENT_PARMS


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_16_SPARSE_DATA \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_16_RPC_WRAPPERS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_16_CALLBACK_WRAPPERS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_16_INCLASS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_16_SPARSE_DATA \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_16_CALLBACK_WRAPPERS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_16_INCLASS_NO_PURE_DECLS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AIRUNTIME_API UClass* StaticClass<class UCoverCombatAction>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
