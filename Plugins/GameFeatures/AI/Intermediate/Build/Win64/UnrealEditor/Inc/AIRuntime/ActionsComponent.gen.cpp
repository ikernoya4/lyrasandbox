// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AIRuntime/Public/Components/ActionsComponent.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeActionsComponent() {}
// Cross Module References
	AIRUNTIME_API UClass* Z_Construct_UClass_UActionsComponent_NoRegister();
	AIRUNTIME_API UClass* Z_Construct_UClass_UActionsComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_AIRuntime();
	AIRUNTIME_API UClass* Z_Construct_UClass_UAgentData_NoRegister();
	AIRUNTIME_API UClass* Z_Construct_UClass_UGActionBase_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UActionsComponent::execResetActions)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ResetActions();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UActionsComponent::execInitializeActions)
	{
		P_GET_OBJECT(UAgentData,Z_Param_Data);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->InitializeActions(Z_Param_Data);
		P_NATIVE_END;
	}
	void UActionsComponent::StaticRegisterNativesUActionsComponent()
	{
		UClass* Class = UActionsComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "InitializeActions", &UActionsComponent::execInitializeActions },
			{ "ResetActions", &UActionsComponent::execResetActions },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UActionsComponent_InitializeActions_Statics
	{
		struct ActionsComponent_eventInitializeActions_Parms
		{
			UAgentData* Data;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Data;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UActionsComponent_InitializeActions_Statics::NewProp_Data = { "Data", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ActionsComponent_eventInitializeActions_Parms, Data), Z_Construct_UClass_UAgentData_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UActionsComponent_InitializeActions_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UActionsComponent_InitializeActions_Statics::NewProp_Data,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UActionsComponent_InitializeActions_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Components/ActionsComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UActionsComponent_InitializeActions_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UActionsComponent, nullptr, "InitializeActions", nullptr, nullptr, sizeof(Z_Construct_UFunction_UActionsComponent_InitializeActions_Statics::ActionsComponent_eventInitializeActions_Parms), Z_Construct_UFunction_UActionsComponent_InitializeActions_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UActionsComponent_InitializeActions_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UActionsComponent_InitializeActions_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UActionsComponent_InitializeActions_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UActionsComponent_InitializeActions()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UActionsComponent_InitializeActions_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UActionsComponent_ResetActions_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UActionsComponent_ResetActions_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Components/ActionsComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UActionsComponent_ResetActions_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UActionsComponent, nullptr, "ResetActions", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UActionsComponent_ResetActions_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UActionsComponent_ResetActions_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UActionsComponent_ResetActions()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UActionsComponent_ResetActions_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UActionsComponent);
	UClass* Z_Construct_UClass_UActionsComponent_NoRegister()
	{
		return UActionsComponent::StaticClass();
	}
	struct Z_Construct_UClass_UActionsComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Actions_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Actions_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_Actions;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UActionsComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_AIRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UActionsComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UActionsComponent_InitializeActions, "InitializeActions" }, // 1836705656
		{ &Z_Construct_UFunction_UActionsComponent_ResetActions, "ResetActions" }, // 3708045938
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActionsComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "Components/ActionsComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Components/ActionsComponent.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UActionsComponent_Statics::NewProp_Actions_Inner = { "Actions", nullptr, (EPropertyFlags)0x0000000000020000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UGActionBase_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActionsComponent_Statics::NewProp_Actions_MetaData[] = {
		{ "Category", "ActionsComponent" },
		{ "ModuleRelativePath", "Public/Components/ActionsComponent.h" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UActionsComponent_Statics::NewProp_Actions = { "Actions", nullptr, (EPropertyFlags)0x0010000000020015, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UActionsComponent, Actions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UActionsComponent_Statics::NewProp_Actions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UActionsComponent_Statics::NewProp_Actions_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UActionsComponent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActionsComponent_Statics::NewProp_Actions_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActionsComponent_Statics::NewProp_Actions,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UActionsComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UActionsComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UActionsComponent_Statics::ClassParams = {
		&UActionsComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UActionsComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UActionsComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UActionsComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UActionsComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UActionsComponent()
	{
		if (!Z_Registration_Info_UClass_UActionsComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UActionsComponent.OuterSingleton, Z_Construct_UClass_UActionsComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UActionsComponent.OuterSingleton;
	}
	template<> AIRUNTIME_API UClass* StaticClass<UActionsComponent>()
	{
		return UActionsComponent::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UActionsComponent);
	struct Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_ActionsComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_ActionsComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UActionsComponent, UActionsComponent::StaticClass, TEXT("UActionsComponent"), &Z_Registration_Info_UClass_UActionsComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UActionsComponent), 1799329638U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_ActionsComponent_h_3025131067(TEXT("/Script/AIRuntime"),
		Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_ActionsComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_Components_ActionsComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
