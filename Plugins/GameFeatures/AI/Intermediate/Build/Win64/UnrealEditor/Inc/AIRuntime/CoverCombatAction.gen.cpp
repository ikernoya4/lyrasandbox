// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AIRuntime/Actions/CoverCombatAction.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCoverCombatAction() {}
// Cross Module References
	AIRUNTIME_API UClass* Z_Construct_UClass_UCoverCombatAction_NoRegister();
	AIRUNTIME_API UClass* Z_Construct_UClass_UCoverCombatAction();
	AIRUNTIME_API UClass* Z_Construct_UClass_UGActionBase();
	UPackage* Z_Construct_UPackage__Script_AIRuntime();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_EObjectTypeQuery();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_EDrawDebugTrace();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
// End Cross Module References
	DEFINE_FUNCTION(UCoverCombatAction::execGetBackToCover)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetBackToCover();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCoverCombatAction::execMoveOutOfCover)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->MoveOutOfCover();
		P_NATIVE_END;
	}
	static FName NAME_UCoverCombatAction_CheckOffsetPosition = FName(TEXT("CheckOffsetPosition"));
	bool UCoverCombatAction::CheckOffsetPosition(FVector LeftOffset, FVector RightOffset, FVector HeightOffset, AActor* Enemy)
	{
		CoverCombatAction_eventCheckOffsetPosition_Parms Parms;
		Parms.LeftOffset=LeftOffset;
		Parms.RightOffset=RightOffset;
		Parms.HeightOffset=HeightOffset;
		Parms.Enemy=Enemy;
		ProcessEvent(FindFunctionChecked(NAME_UCoverCombatAction_CheckOffsetPosition),&Parms);
		return !!Parms.ReturnValue;
	}
	void UCoverCombatAction::StaticRegisterNativesUCoverCombatAction()
	{
		UClass* Class = UCoverCombatAction::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetBackToCover", &UCoverCombatAction::execGetBackToCover },
			{ "MoveOutOfCover", &UCoverCombatAction::execMoveOutOfCover },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCoverCombatAction_CheckOffsetPosition_Statics
	{
		static const UECodeGen_Private::FStructPropertyParams NewProp_LeftOffset;
		static const UECodeGen_Private::FStructPropertyParams NewProp_RightOffset;
		static const UECodeGen_Private::FStructPropertyParams NewProp_HeightOffset;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Enemy;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCoverCombatAction_CheckOffsetPosition_Statics::NewProp_LeftOffset = { "LeftOffset", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CoverCombatAction_eventCheckOffsetPosition_Parms, LeftOffset), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCoverCombatAction_CheckOffsetPosition_Statics::NewProp_RightOffset = { "RightOffset", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CoverCombatAction_eventCheckOffsetPosition_Parms, RightOffset), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCoverCombatAction_CheckOffsetPosition_Statics::NewProp_HeightOffset = { "HeightOffset", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CoverCombatAction_eventCheckOffsetPosition_Parms, HeightOffset), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCoverCombatAction_CheckOffsetPosition_Statics::NewProp_Enemy = { "Enemy", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CoverCombatAction_eventCheckOffsetPosition_Parms, Enemy), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UCoverCombatAction_CheckOffsetPosition_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((CoverCombatAction_eventCheckOffsetPosition_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCoverCombatAction_CheckOffsetPosition_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CoverCombatAction_eventCheckOffsetPosition_Parms), &Z_Construct_UFunction_UCoverCombatAction_CheckOffsetPosition_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCoverCombatAction_CheckOffsetPosition_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCoverCombatAction_CheckOffsetPosition_Statics::NewProp_LeftOffset,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCoverCombatAction_CheckOffsetPosition_Statics::NewProp_RightOffset,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCoverCombatAction_CheckOffsetPosition_Statics::NewProp_HeightOffset,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCoverCombatAction_CheckOffsetPosition_Statics::NewProp_Enemy,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCoverCombatAction_CheckOffsetPosition_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCoverCombatAction_CheckOffsetPosition_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Actions/CoverCombatAction.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCoverCombatAction_CheckOffsetPosition_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCoverCombatAction, nullptr, "CheckOffsetPosition", nullptr, nullptr, sizeof(CoverCombatAction_eventCheckOffsetPosition_Parms), Z_Construct_UFunction_UCoverCombatAction_CheckOffsetPosition_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCoverCombatAction_CheckOffsetPosition_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08880800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCoverCombatAction_CheckOffsetPosition_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCoverCombatAction_CheckOffsetPosition_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCoverCombatAction_CheckOffsetPosition()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCoverCombatAction_CheckOffsetPosition_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCoverCombatAction_GetBackToCover_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCoverCombatAction_GetBackToCover_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Actions/CoverCombatAction.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCoverCombatAction_GetBackToCover_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCoverCombatAction, nullptr, "GetBackToCover", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCoverCombatAction_GetBackToCover_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCoverCombatAction_GetBackToCover_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCoverCombatAction_GetBackToCover()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCoverCombatAction_GetBackToCover_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCoverCombatAction_MoveOutOfCover_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCoverCombatAction_MoveOutOfCover_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Actions/CoverCombatAction.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCoverCombatAction_MoveOutOfCover_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCoverCombatAction, nullptr, "MoveOutOfCover", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCoverCombatAction_MoveOutOfCover_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCoverCombatAction_MoveOutOfCover_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCoverCombatAction_MoveOutOfCover()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCoverCombatAction_MoveOutOfCover_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UCoverCombatAction);
	UClass* Z_Construct_UClass_UCoverCombatAction_NoRegister()
	{
		return UCoverCombatAction::StaticClass();
	}
	struct Z_Construct_UClass_UCoverCombatAction_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_ChosenPositions_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ChosenPositions_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ChosenPositions;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_MoveOffset_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_MoveOffset;
		static const UECodeGen_Private::FBytePropertyParams NewProp_ObjectTypes_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ObjectTypes_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ObjectTypes;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_LineTraceDrawType_MetaData[];
#endif
		static const UECodeGen_Private::FBytePropertyParams NewProp_LineTraceDrawType;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_MinMaxTimeToAttack_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_MinMaxTimeToAttack;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_MinMaxTimeToTakeCover_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_MinMaxTimeToTakeCover;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCoverCombatAction_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGActionBase,
		(UObject* (*)())Z_Construct_UPackage__Script_AIRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCoverCombatAction_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCoverCombatAction_CheckOffsetPosition, "CheckOffsetPosition" }, // 2622092359
		{ &Z_Construct_UFunction_UCoverCombatAction_GetBackToCover, "GetBackToCover" }, // 2381459804
		{ &Z_Construct_UFunction_UCoverCombatAction_MoveOutOfCover, "MoveOutOfCover" }, // 1699701441
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCoverCombatAction_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "Actions/CoverCombatAction.h" },
		{ "ModuleRelativePath", "Actions/CoverCombatAction.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_ChosenPositions_Inner = { "ChosenPositions", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_ChosenPositions_MetaData[] = {
		{ "Category", "CoverCombatAction" },
		{ "ModuleRelativePath", "Actions/CoverCombatAction.h" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_ChosenPositions = { "ChosenPositions", nullptr, (EPropertyFlags)0x0020080000000005, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCoverCombatAction, ChosenPositions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_ChosenPositions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_ChosenPositions_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_MoveOffset_MetaData[] = {
		{ "Category", "CoverCombatAction" },
		{ "ModuleRelativePath", "Actions/CoverCombatAction.h" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_MoveOffset = { "MoveOffset", nullptr, (EPropertyFlags)0x0020080000000005, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCoverCombatAction, MoveOffset), METADATA_PARAMS(Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_MoveOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_MoveOffset_MetaData)) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_ObjectTypes_Inner = { "ObjectTypes", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UEnum_Engine_EObjectTypeQuery, METADATA_PARAMS(nullptr, 0) }; // 3629342158
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_ObjectTypes_MetaData[] = {
		{ "Category", "CoverCombatAction" },
		{ "ModuleRelativePath", "Actions/CoverCombatAction.h" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_ObjectTypes = { "ObjectTypes", nullptr, (EPropertyFlags)0x0020080000000005, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCoverCombatAction, ObjectTypes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_ObjectTypes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_ObjectTypes_MetaData)) }; // 3629342158
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_LineTraceDrawType_MetaData[] = {
		{ "Category", "CoverCombatAction" },
		{ "ModuleRelativePath", "Actions/CoverCombatAction.h" },
	};
#endif
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_LineTraceDrawType = { "LineTraceDrawType", nullptr, (EPropertyFlags)0x0020080000000005, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCoverCombatAction, LineTraceDrawType), Z_Construct_UEnum_Engine_EDrawDebugTrace, METADATA_PARAMS(Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_LineTraceDrawType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_LineTraceDrawType_MetaData)) }; // 2158289653
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_MinMaxTimeToAttack_MetaData[] = {
		{ "Category", "CoverCombatAction" },
		{ "ModuleRelativePath", "Actions/CoverCombatAction.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_MinMaxTimeToAttack = { "MinMaxTimeToAttack", nullptr, (EPropertyFlags)0x0020080000000015, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCoverCombatAction, MinMaxTimeToAttack), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_MinMaxTimeToAttack_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_MinMaxTimeToAttack_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_MinMaxTimeToTakeCover_MetaData[] = {
		{ "Category", "CoverCombatAction" },
		{ "ModuleRelativePath", "Actions/CoverCombatAction.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_MinMaxTimeToTakeCover = { "MinMaxTimeToTakeCover", nullptr, (EPropertyFlags)0x0020080000000015, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCoverCombatAction, MinMaxTimeToTakeCover), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_MinMaxTimeToTakeCover_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_MinMaxTimeToTakeCover_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCoverCombatAction_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_ChosenPositions_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_ChosenPositions,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_MoveOffset,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_ObjectTypes_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_ObjectTypes,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_LineTraceDrawType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_MinMaxTimeToAttack,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCoverCombatAction_Statics::NewProp_MinMaxTimeToTakeCover,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCoverCombatAction_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCoverCombatAction>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UCoverCombatAction_Statics::ClassParams = {
		&UCoverCombatAction::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCoverCombatAction_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCoverCombatAction_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCoverCombatAction_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCoverCombatAction_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCoverCombatAction()
	{
		if (!Z_Registration_Info_UClass_UCoverCombatAction.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UCoverCombatAction.OuterSingleton, Z_Construct_UClass_UCoverCombatAction_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UCoverCombatAction.OuterSingleton;
	}
	template<> AIRUNTIME_API UClass* StaticClass<UCoverCombatAction>()
	{
		return UCoverCombatAction::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCoverCombatAction);
	struct Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UCoverCombatAction, UCoverCombatAction::StaticClass, TEXT("UCoverCombatAction"), &Z_Registration_Info_UClass_UCoverCombatAction, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UCoverCombatAction), 2653803536U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_2254252170(TEXT("/Script/AIRuntime"),
		Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_CoverCombatAction_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
