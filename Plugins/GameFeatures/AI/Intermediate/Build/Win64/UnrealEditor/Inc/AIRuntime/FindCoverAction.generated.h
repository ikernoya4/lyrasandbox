// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FAIRequestID;
#ifdef AIRUNTIME_FindCoverAction_generated_h
#error "FindCoverAction.generated.h already included, missing '#pragma once' in FindCoverAction.h"
#endif
#define AIRUNTIME_FindCoverAction_generated_h

#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_FindCoverAction_h_18_SPARSE_DATA
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_FindCoverAction_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnDestinationReached);


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_FindCoverAction_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnDestinationReached);


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_FindCoverAction_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFindCoverAction(); \
	friend struct Z_Construct_UClass_UFindCoverAction_Statics; \
public: \
	DECLARE_CLASS(UFindCoverAction, UGActionBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AIRuntime"), NO_API) \
	DECLARE_SERIALIZER(UFindCoverAction)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_FindCoverAction_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUFindCoverAction(); \
	friend struct Z_Construct_UClass_UFindCoverAction_Statics; \
public: \
	DECLARE_CLASS(UFindCoverAction, UGActionBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AIRuntime"), NO_API) \
	DECLARE_SERIALIZER(UFindCoverAction)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_FindCoverAction_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFindCoverAction(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFindCoverAction) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFindCoverAction); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFindCoverAction); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFindCoverAction(UFindCoverAction&&); \
	NO_API UFindCoverAction(const UFindCoverAction&); \
public:


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_FindCoverAction_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFindCoverAction(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFindCoverAction(UFindCoverAction&&); \
	NO_API UFindCoverAction(const UFindCoverAction&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFindCoverAction); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFindCoverAction); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFindCoverAction)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_FindCoverAction_h_15_PROLOG
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_FindCoverAction_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_FindCoverAction_h_18_SPARSE_DATA \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_FindCoverAction_h_18_RPC_WRAPPERS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_FindCoverAction_h_18_INCLASS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_FindCoverAction_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_FindCoverAction_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_FindCoverAction_h_18_SPARSE_DATA \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_FindCoverAction_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_FindCoverAction_h_18_INCLASS_NO_PURE_DECLS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_FindCoverAction_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AIRUNTIME_API UClass* StaticClass<class UFindCoverAction>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Actions_FindCoverAction_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
