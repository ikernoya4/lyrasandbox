// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AIRUNTIME_AgentData_generated_h
#error "AgentData.generated.h already included, missing '#pragma once' in AgentData.h"
#endif
#define AIRUNTIME_AgentData_generated_h

#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_AgentData_h_17_SPARSE_DATA
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_AgentData_h_17_RPC_WRAPPERS
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_AgentData_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_AgentData_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAgentData(); \
	friend struct Z_Construct_UClass_UAgentData_Statics; \
public: \
	DECLARE_CLASS(UAgentData, UPrimaryDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AIRuntime"), NO_API) \
	DECLARE_SERIALIZER(UAgentData)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_AgentData_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUAgentData(); \
	friend struct Z_Construct_UClass_UAgentData_Statics; \
public: \
	DECLARE_CLASS(UAgentData, UPrimaryDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AIRuntime"), NO_API) \
	DECLARE_SERIALIZER(UAgentData)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_AgentData_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAgentData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAgentData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAgentData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAgentData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAgentData(UAgentData&&); \
	NO_API UAgentData(const UAgentData&); \
public:


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_AgentData_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAgentData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAgentData(UAgentData&&); \
	NO_API UAgentData(const UAgentData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAgentData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAgentData); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAgentData)


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_AgentData_h_14_PROLOG
#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_AgentData_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_AgentData_h_17_SPARSE_DATA \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_AgentData_h_17_RPC_WRAPPERS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_AgentData_h_17_INCLASS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_AgentData_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_AgentData_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_AgentData_h_17_SPARSE_DATA \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_AgentData_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_AgentData_h_17_INCLASS_NO_PURE_DECLS \
	FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_AgentData_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AIRUNTIME_API UClass* StaticClass<class UAgentData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_AgentData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
