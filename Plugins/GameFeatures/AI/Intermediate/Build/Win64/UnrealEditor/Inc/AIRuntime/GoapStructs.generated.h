// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AIRUNTIME_GoapStructs_generated_h
#error "GoapStructs.generated.h already included, missing '#pragma once' in GoapStructs.h"
#endif
#define AIRUNTIME_GoapStructs_generated_h

#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_GoapStructs_h_17_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FWorldState_Statics; \
	AIRUNTIME_API static class UScriptStruct* StaticStruct();


template<> AIRUNTIME_API UScriptStruct* StaticStruct<struct FWorldState>();

#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_GoapStructs_h_30_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FWorldStates_Statics; \
	AIRUNTIME_API static class UScriptStruct* StaticStruct();


template<> AIRUNTIME_API UScriptStruct* StaticStruct<struct FWorldStates>();

#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_GoapStructs_h_61_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FGoapAction_Statics; \
	AIRUNTIME_API static class UScriptStruct* StaticStruct();


template<> AIRUNTIME_API UScriptStruct* StaticStruct<struct FGoapAction>();

#define FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_GoapStructs_h_100_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSubGoal_Statics; \
	AIRUNTIME_API static class UScriptStruct* StaticStruct();


template<> AIRUNTIME_API UScriptStruct* StaticStruct<struct FSubGoal>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_GoapStructs_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
