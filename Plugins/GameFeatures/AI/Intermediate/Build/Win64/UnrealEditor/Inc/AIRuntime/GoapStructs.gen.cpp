// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AIRuntime/Data/GoapStructs.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGoapStructs() {}
// Cross Module References
	AIRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FWorldState();
	UPackage* Z_Construct_UPackage__Script_AIRuntime();
	AIRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FWorldStates();
	AIRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FGoapAction();
	GAMEPLAYTAGS_API UScriptStruct* Z_Construct_UScriptStruct_FGameplayTag();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	AIRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FSubGoal();
// End Cross Module References
	static FStructRegistrationInfo Z_Registration_Info_UScriptStruct_WorldState;
class UScriptStruct* FWorldState::StaticStruct()
{
	if (!Z_Registration_Info_UScriptStruct_WorldState.OuterSingleton)
	{
		Z_Registration_Info_UScriptStruct_WorldState.OuterSingleton = GetStaticStruct(Z_Construct_UScriptStruct_FWorldState, Z_Construct_UPackage__Script_AIRuntime(), TEXT("WorldState"));
	}
	return Z_Registration_Info_UScriptStruct_WorldState.OuterSingleton;
}
template<> AIRUNTIME_API UScriptStruct* StaticStruct<FWorldState>()
{
	return FWorldState::StaticStruct();
}
	struct Z_Construct_UScriptStruct_FWorldState_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UECodeGen_Private::FNamePropertyParams NewProp_Key;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static void NewProp_Value_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_Value;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWorldState_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * \n */" },
		{ "ModuleRelativePath", "Data/GoapStructs.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FWorldState_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWorldState>();
	}
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWorldState_Statics::NewProp_Key_MetaData[] = {
		{ "Category", "WorldState" },
		{ "ModuleRelativePath", "Data/GoapStructs.h" },
	};
#endif
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FWorldState_Statics::NewProp_Key = { "Key", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWorldState, Key), METADATA_PARAMS(Z_Construct_UScriptStruct_FWorldState_Statics::NewProp_Key_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWorldState_Statics::NewProp_Key_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWorldState_Statics::NewProp_Value_MetaData[] = {
		{ "Category", "WorldState" },
		{ "ModuleRelativePath", "Data/GoapStructs.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FWorldState_Statics::NewProp_Value_SetBit(void* Obj)
	{
		((FWorldState*)Obj)->Value = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FWorldState_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FWorldState), &Z_Construct_UScriptStruct_FWorldState_Statics::NewProp_Value_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FWorldState_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWorldState_Statics::NewProp_Value_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWorldState_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWorldState_Statics::NewProp_Key,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWorldState_Statics::NewProp_Value,
	};
	const UECodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWorldState_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AIRuntime,
		nullptr,
		&NewStructOps,
		"WorldState",
		sizeof(FWorldState),
		alignof(FWorldState),
		Z_Construct_UScriptStruct_FWorldState_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWorldState_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWorldState_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWorldState_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWorldState()
	{
		if (!Z_Registration_Info_UScriptStruct_WorldState.InnerSingleton)
		{
			UECodeGen_Private::ConstructUScriptStruct(Z_Registration_Info_UScriptStruct_WorldState.InnerSingleton, Z_Construct_UScriptStruct_FWorldState_Statics::ReturnStructParams);
		}
		return Z_Registration_Info_UScriptStruct_WorldState.InnerSingleton;
	}
	static FStructRegistrationInfo Z_Registration_Info_UScriptStruct_WorldStates;
class UScriptStruct* FWorldStates::StaticStruct()
{
	if (!Z_Registration_Info_UScriptStruct_WorldStates.OuterSingleton)
	{
		Z_Registration_Info_UScriptStruct_WorldStates.OuterSingleton = GetStaticStruct(Z_Construct_UScriptStruct_FWorldStates, Z_Construct_UPackage__Script_AIRuntime(), TEXT("WorldStates"));
	}
	return Z_Registration_Info_UScriptStruct_WorldStates.OuterSingleton;
}
template<> AIRUNTIME_API UScriptStruct* StaticStruct<FWorldStates>()
{
	return FWorldStates::StaticStruct();
}
	struct Z_Construct_UScriptStruct_FWorldStates_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UECodeGen_Private::FBoolPropertyParams NewProp_States_ValueProp;
		static const UECodeGen_Private::FNamePropertyParams NewProp_States_Key_KeyProp;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_States_MetaData[];
#endif
		static const UECodeGen_Private::FMapPropertyParams NewProp_States;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWorldStates_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Data/GoapStructs.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FWorldStates_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWorldStates>();
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FWorldStates_Statics::NewProp_States_ValueProp = { "States", nullptr, (EPropertyFlags)0x0000000000000001, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FWorldStates_Statics::NewProp_States_Key_KeyProp = { "States_Key", nullptr, (EPropertyFlags)0x0000000000000001, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWorldStates_Statics::NewProp_States_MetaData[] = {
		{ "Category", "WorldState" },
		{ "ModuleRelativePath", "Data/GoapStructs.h" },
	};
#endif
	const UECodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FWorldStates_Statics::NewProp_States = { "States", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWorldStates, States), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FWorldStates_Statics::NewProp_States_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWorldStates_Statics::NewProp_States_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWorldStates_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWorldStates_Statics::NewProp_States_ValueProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWorldStates_Statics::NewProp_States_Key_KeyProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWorldStates_Statics::NewProp_States,
	};
	const UECodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWorldStates_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AIRuntime,
		nullptr,
		&NewStructOps,
		"WorldStates",
		sizeof(FWorldStates),
		alignof(FWorldStates),
		Z_Construct_UScriptStruct_FWorldStates_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWorldStates_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWorldStates_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWorldStates_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWorldStates()
	{
		if (!Z_Registration_Info_UScriptStruct_WorldStates.InnerSingleton)
		{
			UECodeGen_Private::ConstructUScriptStruct(Z_Registration_Info_UScriptStruct_WorldStates.InnerSingleton, Z_Construct_UScriptStruct_FWorldStates_Statics::ReturnStructParams);
		}
		return Z_Registration_Info_UScriptStruct_WorldStates.InnerSingleton;
	}
	static FStructRegistrationInfo Z_Registration_Info_UScriptStruct_GoapAction;
class UScriptStruct* FGoapAction::StaticStruct()
{
	if (!Z_Registration_Info_UScriptStruct_GoapAction.OuterSingleton)
	{
		Z_Registration_Info_UScriptStruct_GoapAction.OuterSingleton = GetStaticStruct(Z_Construct_UScriptStruct_FGoapAction, Z_Construct_UPackage__Script_AIRuntime(), TEXT("GoapAction"));
	}
	return Z_Registration_Info_UScriptStruct_GoapAction.OuterSingleton;
}
template<> AIRUNTIME_API UScriptStruct* StaticStruct<FGoapAction>()
{
	return FGoapAction::StaticStruct();
}
	struct Z_Construct_UScriptStruct_FGoapAction_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ActionName_MetaData[];
#endif
		static const UECodeGen_Private::FNamePropertyParams NewProp_ActionName;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ActionTag_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_ActionTag;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Cost_MetaData[];
#endif
		static const UECodeGen_Private::FIntPropertyParams NewProp_Cost;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Target_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Target;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Duration_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_Duration;
		static const UECodeGen_Private::FStructPropertyParams NewProp_Preconditions_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Preconditions_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_Preconditions;
		static const UECodeGen_Private::FStructPropertyParams NewProp_Effects_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Effects_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_Effects;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_AgentBeliefs_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_AgentBeliefs;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_bRunning_MetaData[];
#endif
		static void NewProp_bRunning_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bRunning;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGoapAction_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Data/GoapStructs.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FGoapAction_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FGoapAction>();
	}
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_ActionName_MetaData[] = {
		{ "Category", "GoapAction" },
		{ "ModuleRelativePath", "Data/GoapStructs.h" },
	};
#endif
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_ActionName = { "ActionName", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGoapAction, ActionName), METADATA_PARAMS(Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_ActionName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_ActionName_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_ActionTag_MetaData[] = {
		{ "Category", "GoapAction" },
		{ "ModuleRelativePath", "Data/GoapStructs.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_ActionTag = { "ActionTag", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGoapAction, ActionTag), Z_Construct_UScriptStruct_FGameplayTag, METADATA_PARAMS(Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_ActionTag_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_ActionTag_MetaData)) }; // 802167388
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Cost_MetaData[] = {
		{ "Category", "GoapAction" },
		{ "ModuleRelativePath", "Data/GoapStructs.h" },
	};
#endif
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Cost = { "Cost", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGoapAction, Cost), METADATA_PARAMS(Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Cost_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Cost_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Target_MetaData[] = {
		{ "Category", "GoapAction" },
		{ "ModuleRelativePath", "Data/GoapStructs.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Target = { "Target", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGoapAction, Target), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Target_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Target_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Duration_MetaData[] = {
		{ "Category", "GoapAction" },
		{ "ModuleRelativePath", "Data/GoapStructs.h" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Duration = { "Duration", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGoapAction, Duration), METADATA_PARAMS(Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Duration_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Duration_MetaData)) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Preconditions_Inner = { "Preconditions", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FWorldState, METADATA_PARAMS(nullptr, 0) }; // 948963605
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Preconditions_MetaData[] = {
		{ "Category", "GoapAction" },
		{ "ModuleRelativePath", "Data/GoapStructs.h" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Preconditions = { "Preconditions", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGoapAction, Preconditions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Preconditions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Preconditions_MetaData)) }; // 948963605
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Effects_Inner = { "Effects", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FWorldState, METADATA_PARAMS(nullptr, 0) }; // 948963605
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Effects_MetaData[] = {
		{ "Category", "GoapAction" },
		{ "ModuleRelativePath", "Data/GoapStructs.h" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Effects = { "Effects", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGoapAction, Effects), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Effects_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Effects_MetaData)) }; // 948963605
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_AgentBeliefs_MetaData[] = {
		{ "Category", "GoapAction" },
		{ "ModuleRelativePath", "Data/GoapStructs.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_AgentBeliefs = { "AgentBeliefs", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGoapAction, AgentBeliefs), Z_Construct_UScriptStruct_FWorldStates, METADATA_PARAMS(Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_AgentBeliefs_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_AgentBeliefs_MetaData)) }; // 21332049
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_bRunning_MetaData[] = {
		{ "Category", "GoapAction" },
		{ "ModuleRelativePath", "Data/GoapStructs.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_bRunning_SetBit(void* Obj)
	{
		((FGoapAction*)Obj)->bRunning = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_bRunning = { "bRunning", nullptr, (EPropertyFlags)0x0010000000020005, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FGoapAction), &Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_bRunning_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_bRunning_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_bRunning_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FGoapAction_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_ActionName,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_ActionTag,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Cost,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Target,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Duration,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Preconditions_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Preconditions,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Effects_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_Effects,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_AgentBeliefs,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGoapAction_Statics::NewProp_bRunning,
	};
	const UECodeGen_Private::FStructParams Z_Construct_UScriptStruct_FGoapAction_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AIRuntime,
		nullptr,
		&NewStructOps,
		"GoapAction",
		sizeof(FGoapAction),
		alignof(FGoapAction),
		Z_Construct_UScriptStruct_FGoapAction_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGoapAction_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FGoapAction_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGoapAction_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FGoapAction()
	{
		if (!Z_Registration_Info_UScriptStruct_GoapAction.InnerSingleton)
		{
			UECodeGen_Private::ConstructUScriptStruct(Z_Registration_Info_UScriptStruct_GoapAction.InnerSingleton, Z_Construct_UScriptStruct_FGoapAction_Statics::ReturnStructParams);
		}
		return Z_Registration_Info_UScriptStruct_GoapAction.InnerSingleton;
	}
	static FStructRegistrationInfo Z_Registration_Info_UScriptStruct_SubGoal;
class UScriptStruct* FSubGoal::StaticStruct()
{
	if (!Z_Registration_Info_UScriptStruct_SubGoal.OuterSingleton)
	{
		Z_Registration_Info_UScriptStruct_SubGoal.OuterSingleton = GetStaticStruct(Z_Construct_UScriptStruct_FSubGoal, Z_Construct_UPackage__Script_AIRuntime(), TEXT("SubGoal"));
	}
	return Z_Registration_Info_UScriptStruct_SubGoal.OuterSingleton;
}
template<> AIRUNTIME_API UScriptStruct* StaticStruct<FSubGoal>()
{
	return FSubGoal::StaticStruct();
}
	struct Z_Construct_UScriptStruct_FSubGoal_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UECodeGen_Private::FNamePropertyParams NewProp_Name;
		static const UECodeGen_Private::FBoolPropertyParams NewProp_Goals_ValueProp;
		static const UECodeGen_Private::FNamePropertyParams NewProp_Goals_Key_KeyProp;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Goals_MetaData[];
#endif
		static const UECodeGen_Private::FMapPropertyParams NewProp_Goals;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_bRemove_MetaData[];
#endif
		static void NewProp_bRemove_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bRemove;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSubGoal_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Data/GoapStructs.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSubGoal_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSubGoal>();
	}
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSubGoal_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "SubGoal" },
		{ "ModuleRelativePath", "Data/GoapStructs.h" },
	};
#endif
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FSubGoal_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSubGoal, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FSubGoal_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSubGoal_Statics::NewProp_Name_MetaData)) };
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FSubGoal_Statics::NewProp_Goals_ValueProp = { "Goals", nullptr, (EPropertyFlags)0x0000000000000001, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FSubGoal_Statics::NewProp_Goals_Key_KeyProp = { "Goals_Key", nullptr, (EPropertyFlags)0x0000000000000001, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSubGoal_Statics::NewProp_Goals_MetaData[] = {
		{ "Category", "SubGoal" },
		{ "ModuleRelativePath", "Data/GoapStructs.h" },
	};
#endif
	const UECodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FSubGoal_Statics::NewProp_Goals = { "Goals", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSubGoal, Goals), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FSubGoal_Statics::NewProp_Goals_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSubGoal_Statics::NewProp_Goals_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSubGoal_Statics::NewProp_bRemove_MetaData[] = {
		{ "Category", "SubGoal" },
		{ "ModuleRelativePath", "Data/GoapStructs.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FSubGoal_Statics::NewProp_bRemove_SetBit(void* Obj)
	{
		((FSubGoal*)Obj)->bRemove = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FSubGoal_Statics::NewProp_bRemove = { "bRemove", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FSubGoal), &Z_Construct_UScriptStruct_FSubGoal_Statics::NewProp_bRemove_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FSubGoal_Statics::NewProp_bRemove_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSubGoal_Statics::NewProp_bRemove_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSubGoal_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSubGoal_Statics::NewProp_Name,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSubGoal_Statics::NewProp_Goals_ValueProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSubGoal_Statics::NewProp_Goals_Key_KeyProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSubGoal_Statics::NewProp_Goals,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSubGoal_Statics::NewProp_bRemove,
	};
	const UECodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSubGoal_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AIRuntime,
		nullptr,
		&NewStructOps,
		"SubGoal",
		sizeof(FSubGoal),
		alignof(FSubGoal),
		Z_Construct_UScriptStruct_FSubGoal_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSubGoal_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSubGoal_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSubGoal_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSubGoal()
	{
		if (!Z_Registration_Info_UScriptStruct_SubGoal.InnerSingleton)
		{
			UECodeGen_Private::ConstructUScriptStruct(Z_Registration_Info_UScriptStruct_SubGoal.InnerSingleton, Z_Construct_UScriptStruct_FSubGoal_Statics::ReturnStructParams);
		}
		return Z_Registration_Info_UScriptStruct_SubGoal.InnerSingleton;
	}
	struct Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_GoapStructs_h_Statics
	{
		static const FStructRegisterCompiledInInfo ScriptStructInfo[];
	};
	const FStructRegisterCompiledInInfo Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_GoapStructs_h_Statics::ScriptStructInfo[] = {
		{ FWorldState::StaticStruct, Z_Construct_UScriptStruct_FWorldState_Statics::NewStructOps, TEXT("WorldState"), &Z_Registration_Info_UScriptStruct_WorldState, CONSTRUCT_RELOAD_VERSION_INFO(FStructReloadVersionInfo, sizeof(FWorldState), 948963605U) },
		{ FWorldStates::StaticStruct, Z_Construct_UScriptStruct_FWorldStates_Statics::NewStructOps, TEXT("WorldStates"), &Z_Registration_Info_UScriptStruct_WorldStates, CONSTRUCT_RELOAD_VERSION_INFO(FStructReloadVersionInfo, sizeof(FWorldStates), 21332049U) },
		{ FGoapAction::StaticStruct, Z_Construct_UScriptStruct_FGoapAction_Statics::NewStructOps, TEXT("GoapAction"), &Z_Registration_Info_UScriptStruct_GoapAction, CONSTRUCT_RELOAD_VERSION_INFO(FStructReloadVersionInfo, sizeof(FGoapAction), 355953289U) },
		{ FSubGoal::StaticStruct, Z_Construct_UScriptStruct_FSubGoal_Statics::NewStructOps, TEXT("SubGoal"), &Z_Registration_Info_UScriptStruct_SubGoal, CONSTRUCT_RELOAD_VERSION_INFO(FStructReloadVersionInfo, sizeof(FSubGoal), 1865115705U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_GoapStructs_h_1303427293(TEXT("/Script/AIRuntime"),
		nullptr, 0,
		Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_GoapStructs_h_Statics::ScriptStructInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_GoapStructs_h_Statics::ScriptStructInfo),
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
