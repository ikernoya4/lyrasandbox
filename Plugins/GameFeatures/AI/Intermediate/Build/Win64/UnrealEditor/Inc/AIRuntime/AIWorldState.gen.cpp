// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AIRuntime/Public/AIWorldState.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAIWorldState() {}
// Cross Module References
	AIRUNTIME_API UFunction* Z_Construct_UDelegateFunction_AIRuntime_OnStateChanged__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_AIRuntime();
	AIRUNTIME_API UClass* Z_Construct_UClass_UAIWorldState_NoRegister();
	AIRUNTIME_API UClass* Z_Construct_UClass_UAIWorldState();
	MODULARGAMEPLAY_API UClass* Z_Construct_UClass_UGameStateComponent();
	AIRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FWorldStates();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_AIRuntime_OnStateChanged__DelegateSignature_Statics
	{
		struct _Script_AIRuntime_eventOnStateChanged_Parms
		{
			FName StateName;
			bool Value;
		};
		static const UECodeGen_Private::FNamePropertyParams NewProp_StateName;
		static void NewProp_Value_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_Value;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UDelegateFunction_AIRuntime_OnStateChanged__DelegateSignature_Statics::NewProp_StateName = { "StateName", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_AIRuntime_eventOnStateChanged_Parms, StateName), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UDelegateFunction_AIRuntime_OnStateChanged__DelegateSignature_Statics::NewProp_Value_SetBit(void* Obj)
	{
		((_Script_AIRuntime_eventOnStateChanged_Parms*)Obj)->Value = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_AIRuntime_OnStateChanged__DelegateSignature_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_AIRuntime_eventOnStateChanged_Parms), &Z_Construct_UDelegateFunction_AIRuntime_OnStateChanged__DelegateSignature_Statics::NewProp_Value_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_AIRuntime_OnStateChanged__DelegateSignature_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_AIRuntime_OnStateChanged__DelegateSignature_Statics::NewProp_StateName,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_AIRuntime_OnStateChanged__DelegateSignature_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_AIRuntime_OnStateChanged__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/AIWorldState.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_AIRuntime_OnStateChanged__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_AIRuntime, nullptr, "OnStateChanged__DelegateSignature", nullptr, nullptr, sizeof(Z_Construct_UDelegateFunction_AIRuntime_OnStateChanged__DelegateSignature_Statics::_Script_AIRuntime_eventOnStateChanged_Parms), Z_Construct_UDelegateFunction_AIRuntime_OnStateChanged__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_AIRuntime_OnStateChanged__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_AIRuntime_OnStateChanged__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_AIRuntime_OnStateChanged__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_AIRuntime_OnStateChanged__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UDelegateFunction_AIRuntime_OnStateChanged__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UAIWorldState::execModifyState)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_Name);
		P_GET_UBOOL(Z_Param_Value);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ModifyState(Z_Param_Name,Z_Param_Value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAIWorldState::execGetWorldState)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FWorldStates*)Z_Param__Result=P_THIS->GetWorldState();
		P_NATIVE_END;
	}
	void UAIWorldState::StaticRegisterNativesUAIWorldState()
	{
		UClass* Class = UAIWorldState::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetWorldState", &UAIWorldState::execGetWorldState },
			{ "ModifyState", &UAIWorldState::execModifyState },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UAIWorldState_GetWorldState_Statics
	{
		struct AIWorldState_eventGetWorldState_Parms
		{
			FWorldStates ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UAIWorldState_GetWorldState_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AIWorldState_eventGetWorldState_Parms, ReturnValue), Z_Construct_UScriptStruct_FWorldStates, METADATA_PARAMS(nullptr, 0) }; // 21332049
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAIWorldState_GetWorldState_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAIWorldState_GetWorldState_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAIWorldState_GetWorldState_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/AIWorldState.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAIWorldState_GetWorldState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAIWorldState, nullptr, "GetWorldState", nullptr, nullptr, sizeof(Z_Construct_UFunction_UAIWorldState_GetWorldState_Statics::AIWorldState_eventGetWorldState_Parms), Z_Construct_UFunction_UAIWorldState_GetWorldState_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAIWorldState_GetWorldState_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAIWorldState_GetWorldState_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAIWorldState_GetWorldState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAIWorldState_GetWorldState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAIWorldState_GetWorldState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAIWorldState_ModifyState_Statics
	{
		struct AIWorldState_eventModifyState_Parms
		{
			FName Name;
			bool Value;
		};
		static const UECodeGen_Private::FNamePropertyParams NewProp_Name;
		static void NewProp_Value_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_Value;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UAIWorldState_ModifyState_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AIWorldState_eventModifyState_Parms, Name), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UAIWorldState_ModifyState_Statics::NewProp_Value_SetBit(void* Obj)
	{
		((AIWorldState_eventModifyState_Parms*)Obj)->Value = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UAIWorldState_ModifyState_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AIWorldState_eventModifyState_Parms), &Z_Construct_UFunction_UAIWorldState_ModifyState_Statics::NewProp_Value_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAIWorldState_ModifyState_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAIWorldState_ModifyState_Statics::NewProp_Name,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAIWorldState_ModifyState_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAIWorldState_ModifyState_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/AIWorldState.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAIWorldState_ModifyState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAIWorldState, nullptr, "ModifyState", nullptr, nullptr, sizeof(Z_Construct_UFunction_UAIWorldState_ModifyState_Statics::AIWorldState_eventModifyState_Parms), Z_Construct_UFunction_UAIWorldState_ModifyState_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAIWorldState_ModifyState_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAIWorldState_ModifyState_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAIWorldState_ModifyState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAIWorldState_ModifyState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAIWorldState_ModifyState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UAIWorldState);
	UClass* Z_Construct_UClass_UAIWorldState_NoRegister()
	{
		return UAIWorldState::StaticClass();
	}
	struct Z_Construct_UClass_UAIWorldState_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_WorldState_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_WorldState;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OnStateChanged_MetaData[];
#endif
		static const UECodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnStateChanged;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAIWorldState_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameStateComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_AIRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UAIWorldState_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UAIWorldState_GetWorldState, "GetWorldState" }, // 1811331863
		{ &Z_Construct_UFunction_UAIWorldState_ModifyState, "ModifyState" }, // 1758033655
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAIWorldState_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "AIWorldState.h" },
		{ "ModuleRelativePath", "Public/AIWorldState.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAIWorldState_Statics::NewProp_WorldState_MetaData[] = {
		{ "Category", "AIWorldState" },
		{ "ModuleRelativePath", "Public/AIWorldState.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UAIWorldState_Statics::NewProp_WorldState = { "WorldState", nullptr, (EPropertyFlags)0x0020080000000015, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAIWorldState, WorldState), Z_Construct_UScriptStruct_FWorldStates, METADATA_PARAMS(Z_Construct_UClass_UAIWorldState_Statics::NewProp_WorldState_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAIWorldState_Statics::NewProp_WorldState_MetaData)) }; // 21332049
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAIWorldState_Statics::NewProp_OnStateChanged_MetaData[] = {
		{ "ModuleRelativePath", "Public/AIWorldState.h" },
	};
#endif
	const UECodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UAIWorldState_Statics::NewProp_OnStateChanged = { "OnStateChanged", nullptr, (EPropertyFlags)0x0010100010080000, UECodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAIWorldState, OnStateChanged), Z_Construct_UDelegateFunction_AIRuntime_OnStateChanged__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UAIWorldState_Statics::NewProp_OnStateChanged_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAIWorldState_Statics::NewProp_OnStateChanged_MetaData)) }; // 3344755026
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAIWorldState_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAIWorldState_Statics::NewProp_WorldState,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAIWorldState_Statics::NewProp_OnStateChanged,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAIWorldState_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAIWorldState>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UAIWorldState_Statics::ClassParams = {
		&UAIWorldState::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UAIWorldState_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UAIWorldState_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UAIWorldState_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAIWorldState_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAIWorldState()
	{
		if (!Z_Registration_Info_UClass_UAIWorldState.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UAIWorldState.OuterSingleton, Z_Construct_UClass_UAIWorldState_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UAIWorldState.OuterSingleton;
	}
	template<> AIRUNTIME_API UClass* StaticClass<UAIWorldState>()
	{
		return UAIWorldState::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAIWorldState);
	struct Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_AIWorldState_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_AIWorldState_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UAIWorldState, UAIWorldState::StaticClass, TEXT("UAIWorldState"), &Z_Registration_Info_UClass_UAIWorldState, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UAIWorldState), 3715034317U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_AIWorldState_h_2628314024(TEXT("/Script/AIRuntime"),
		Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_AIWorldState_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Public_AIWorldState_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
