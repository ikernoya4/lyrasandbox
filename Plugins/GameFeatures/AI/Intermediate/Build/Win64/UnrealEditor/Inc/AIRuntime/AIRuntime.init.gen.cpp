// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAIRuntime_init() {}
	AIRUNTIME_API UFunction* Z_Construct_UDelegateFunction_AIRuntime_OnComplete__DelegateSignature();
	AIRUNTIME_API UFunction* Z_Construct_UDelegateFunction_AIRuntime_OnStateChanged__DelegateSignature();
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_AIRuntime;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_AIRuntime()
	{
		if (!Z_Registration_Info_UPackage__Script_AIRuntime.OuterSingleton)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_AIRuntime_OnComplete__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_AIRuntime_OnStateChanged__DelegateSignature,
			};
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/AIRuntime",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0x66979176,
				0xCEDBB483,
				METADATA_PARAMS(nullptr, 0)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_AIRuntime.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_AIRuntime.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_AIRuntime(Z_Construct_UPackage__Script_AIRuntime, TEXT("/Script/AIRuntime"), Z_Registration_Info_UPackage__Script_AIRuntime, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0x66979176, 0xCEDBB483));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
