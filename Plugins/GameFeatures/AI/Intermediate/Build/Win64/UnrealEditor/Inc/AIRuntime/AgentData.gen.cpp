// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AIRuntime/Data/AgentData.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAgentData() {}
// Cross Module References
	AIRUNTIME_API UClass* Z_Construct_UClass_UAgentData_NoRegister();
	AIRUNTIME_API UClass* Z_Construct_UClass_UAgentData();
	ENGINE_API UClass* Z_Construct_UClass_UPrimaryDataAsset();
	UPackage* Z_Construct_UPackage__Script_AIRuntime();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	AIRUNTIME_API UClass* Z_Construct_UClass_UGActionBase_NoRegister();
	AIRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FSubGoal();
// End Cross Module References
	void UAgentData::StaticRegisterNativesUAgentData()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UAgentData);
	UClass* Z_Construct_UClass_UAgentData_NoRegister()
	{
		return UAgentData::StaticClass();
	}
	struct Z_Construct_UClass_UAgentData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UECodeGen_Private::FNamePropertyParams NewProp_Name;
		static const UECodeGen_Private::FClassPropertyParams NewProp_Actions_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Actions_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_Actions;
		static const UECodeGen_Private::FBoolPropertyParams NewProp_Goals_ValueProp;
		static const UECodeGen_Private::FStructPropertyParams NewProp_Goals_Key_KeyProp;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Goals_MetaData[];
#endif
		static const UECodeGen_Private::FMapPropertyParams NewProp_Goals;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAgentData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UPrimaryDataAsset,
		(UObject* (*)())Z_Construct_UPackage__Script_AIRuntime,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAgentData_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "Data/AgentData.h" },
		{ "ModuleRelativePath", "Data/AgentData.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAgentData_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "AI" },
		{ "ModuleRelativePath", "Data/AgentData.h" },
	};
#endif
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UClass_UAgentData_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAgentData, Name), METADATA_PARAMS(Z_Construct_UClass_UAgentData_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAgentData_Statics::NewProp_Name_MetaData)) };
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UClass_UAgentData_Statics::NewProp_Actions_Inner = { "Actions", nullptr, (EPropertyFlags)0x0004000000000000, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UGActionBase_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAgentData_Statics::NewProp_Actions_MetaData[] = {
		{ "Category", "AI" },
		{ "ModuleRelativePath", "Data/AgentData.h" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UAgentData_Statics::NewProp_Actions = { "Actions", nullptr, (EPropertyFlags)0x0014000000000005, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAgentData, Actions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UAgentData_Statics::NewProp_Actions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAgentData_Statics::NewProp_Actions_MetaData)) };
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAgentData_Statics::NewProp_Goals_ValueProp = { "Goals", nullptr, (EPropertyFlags)0x0000000000000001, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UAgentData_Statics::NewProp_Goals_Key_KeyProp = { "Goals_Key", nullptr, (EPropertyFlags)0x0000000000000001, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSubGoal, METADATA_PARAMS(nullptr, 0) }; // 1865115705
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAgentData_Statics::NewProp_Goals_MetaData[] = {
		{ "Category", "AI" },
		{ "ModuleRelativePath", "Data/AgentData.h" },
	};
#endif
	const UECodeGen_Private::FMapPropertyParams Z_Construct_UClass_UAgentData_Statics::NewProp_Goals = { "Goals", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAgentData, Goals), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UAgentData_Statics::NewProp_Goals_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAgentData_Statics::NewProp_Goals_MetaData)) }; // 1865115705
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAgentData_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAgentData_Statics::NewProp_Name,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAgentData_Statics::NewProp_Actions_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAgentData_Statics::NewProp_Actions,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAgentData_Statics::NewProp_Goals_ValueProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAgentData_Statics::NewProp_Goals_Key_KeyProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAgentData_Statics::NewProp_Goals,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAgentData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAgentData>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UAgentData_Statics::ClassParams = {
		&UAgentData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAgentData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAgentData_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAgentData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAgentData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAgentData()
	{
		if (!Z_Registration_Info_UClass_UAgentData.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UAgentData.OuterSingleton, Z_Construct_UClass_UAgentData_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UAgentData.OuterSingleton;
	}
	template<> AIRUNTIME_API UClass* StaticClass<UAgentData>()
	{
		return UAgentData::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAgentData);
	struct Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_AgentData_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_AgentData_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UAgentData, UAgentData::StaticClass, TEXT("UAgentData"), &Z_Registration_Info_UClass_UAgentData, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UAgentData), 2073568791U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_AgentData_h_3740615042(TEXT("/Script/AIRuntime"),
		Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_AgentData_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_lyrasandbox_Plugins_GameFeatures_AI_Source_AIRuntime_Data_AgentData_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
