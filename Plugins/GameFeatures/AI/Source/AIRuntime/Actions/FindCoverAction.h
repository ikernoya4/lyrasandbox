// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GActionBase.h"
#include "EnvironmentQuery/EnvQuery.h"
#include "EnvironmentQuery/EnvQueryManager.h"
#include "Sandbox/Gameplay/Cover/SandboxCover.h"
#include "FindCoverAction.generated.h"

/**
 * 
 */
UCLASS()
class AIRUNTIME_API UFindCoverAction : public UGActionBase
{
	GENERATED_BODY()

	FEnvQueryRequest QueryRequests;
	
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	ASandboxCover* SelectedCover;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FVector SelectedCoverLocation;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float ClosestDistance = 100000000.f;	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UEnvQuery* FindCoverQuery;
	
	bool bWaitForEQS = true;

public:
	virtual bool PrePerform_Implementation() override;
	virtual void Act_Implementation() override;
	virtual bool PostPerform_Implementation(bool bWasCanceled) override;

private:
	UFUNCTION()
	void OnDestinationReached(FAIRequestID ID, EPathFollowingResult::Type Result);
	
	void OnQueryFinished(TSharedPtr<FEnvQueryResult> Result);
	
};
