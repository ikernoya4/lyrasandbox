// Fill out your copyright notice in the Description page of Project Settings.


#include "CoverCombatAction.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Sandbox/AI/Controllers/SandboxGOAPController.h"
#include "Sandbox/Gameplay/Cover/SandboxCover.h"

bool UCoverCombatAction::PrePerform_Implementation()
{
	bShouldTick=false;
	return Super::PrePerform_Implementation();
}

bool UCoverCombatAction::PostPerform_Implementation(bool bWasCanceled)
{
	GetWorld()->GetTimerManager().ClearTimer(MoveOutHandle);
	GetWorld()->GetTimerManager().ClearTimer(TakeCoverHandle);
	return Super::PostPerform_Implementation(bWasCanceled);
}

void UCoverCombatAction::Act_Implementation()
{
	Super::Act_Implementation();
	ASandboxGOAPController* GoapController = Cast<ASandboxGOAPController>(Controller);
	GoapController->bCanAttack=false;
	float Time = FMath::RandRange(MinMaxTimeToAttack.X,MinMaxTimeToAttack.Y);
	GetWorld()->GetTimerManager().SetTimer(MoveOutHandle, this, &UCoverCombatAction::MoveOutOfCover, Time, false);
}

void UCoverCombatAction::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void UCoverCombatAction::MoveOutOfCover()
{
	ASandboxGOAPController* GoapController = Cast<ASandboxGOAPController>(Controller);
	if(!GoapController || !GoapController->CurrentCover) return;
	switch (GoapController->CurrentCover->GetCoverType())
	{
	case ECoverTypes::Full:
		{
			if(FindAttackAngle())
			{
				Controller->MoveToLocation(ChosenPositions[FMath::RandRange(0, ChosenPositions.Num() - 1)]);
				GoapController->bCanAttack=true;
			}
		}
		break;
	case ECoverTypes::Partial:
		{
			if(auto* Character = Cast<ACharacter>(Controller->GetPawn()))
			{
				Character->UnCrouch();
				GoapController->bCanAttack=true;
			}
		}
		break;
	default: ;
	}
	float Time = FMath::RandRange(MinMaxTimeToTakeCover.X,MinMaxTimeToTakeCover.Y);
	GetWorld()->GetTimerManager().SetTimer(TakeCoverHandle, this, &UCoverCombatAction::GetBackToCover, Time, false);
}

void UCoverCombatAction::GetBackToCover()
{
	ASandboxGOAPController* GoapController = Cast<ASandboxGOAPController>(Controller);
	if(!GoapController || !GoapController->CurrentCover) return;
	switch (GoapController->CurrentCover->GetCoverType())
	{
	case ECoverTypes::Full:
		Controller->MoveToLocation(GoapController->CurrentCover->GetReservedPositionByOwner(Controller->GetPawn()));
		break;
	case ECoverTypes::Partial:
		if(auto* Character = Cast<ACharacter>(Controller->GetPawn()))
		{
			Character->Crouch();
		}
		break;
	default: ;
	}
	GoapController->bCanAttack=false;
	float Time = FMath::RandRange(MinMaxTimeToAttack.X,MinMaxTimeToAttack.Y);
	GetWorld()->GetTimerManager().SetTimer(MoveOutHandle, this, &UCoverCombatAction::MoveOutOfCover, Time, false);
}

bool UCoverCombatAction::FindAttackAngle()
{
	if(!Controller->GetBlackboardComponent()) return false;
	AActor* Enemy = Cast<AActor>(Controller->GetBlackboardComponent()->GetValueAsObject(FName("Target")));
	ASandboxGOAPController* GoapController = Cast<ASandboxGOAPController>(Controller);
	AActor* CoverActor = Cast<AActor>(GoapController->CurrentCover);
	if(!Enemy || !GoapController || !CoverActor) return false;
	ChosenPositions.Empty();
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(Controller->GetPawn());
	Params.AddIgnoredActor(CoverActor);
	TArray<AActor*> IgnoredActors;
	IgnoredActors.Add(CoverActor);
	IgnoredActors.Add(Controller->GetPawn());
	FVector RightOffset = Controller->GetPawn()->GetActorRightVector() * MoveOffset;
	FVector LeftOffset = -Controller->GetPawn()->GetActorRightVector() * MoveOffset;
	FVector HeightOffset = FVector(0,0,60);
	
	return CheckOffsetPosition(LeftOffset, RightOffset, HeightOffset, Enemy);
}
