﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "GameplayTagContainer.h"
#include "AIRuntime/Data/GoapStructs.h"
#include "UObject/Object.h"
#include "GActionBase.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnComplete);

UCLASS(Blueprintable, BlueprintType)
class AIRUNTIME_API UGActionBase : public UObject, public FTickableGameObject
{
	GENERATED_BODY()

	UPROPERTY()
	TMap<FName, bool> Preconditions_Map;
	UPROPERTY()
	TMap<FName, bool> Effects_Map;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	AAIController* Controller = nullptr;
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAction")
	FName ActionName = NAME_None;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAction")
	FGameplayTag ActionTag;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAction")
	int32 Cost = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAction")
	AActor* Target = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAction")
	TSubclassOf<AActor> TargetClass = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAction")
	float Duration = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "GAction|Settings")
	bool bIsInProgress = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAction|Settings")
	TArray<FWorldState> Preconditions;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAction|Settings")
	TArray<FWorldState> Effects;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAction|Settings")
	bool bRunning = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GAction|Settings")
	bool bShouldTick = false;
	
	UPROPERTY(BlueprintAssignable, BlueprintCallable)
	FOnComplete OnComplete;

	UGActionBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	void Init(AAIController* NewController);
	bool IsAchievable();
	bool IsAchievableGivenPreconditions(TMap<FName, bool> Conditions);
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	bool PrePerform();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void Act();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	bool PostPerform(bool bWasCanceled);
	UFUNCTION(BlueprintCallable)
	void CancelAction();
	UFUNCTION(BlueprintNativeEvent)
	void TickAction(float DeltaTime);

	virtual void Tick(float DeltaTime) override;
	virtual bool IsTickable() const override;
	virtual bool IsTickableInEditor() const override;
	virtual bool IsTickableWhenPaused() const override;
	virtual TStatId GetStatId() const override;

	UWorld* GetWorld() const override;
};
