﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "GActionBase.h"

UGActionBase::UGActionBase(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	
}

void UGActionBase::Init(AAIController* NewController) // Reemplazar Pawn con Controller
{
	TArray<FName> PreKeys;
	TArray<FName> EffectKeys;
	Preconditions_Map.GetKeys(PreKeys);
	Effects_Map.GetKeys(EffectKeys);
	if(Preconditions_Map.Num() > 0)
	{
		for(auto Key : PreKeys)
		{
			if(Preconditions_Map.Contains(Key))
			{
				Preconditions_Map.FindAndRemoveChecked(Key);
			}
		}
	}
	if(Effects_Map.Num() > 0)
	{
		for(auto Key : EffectKeys)
		{
			if(Effects_Map.Contains(Key))
			{
				Effects_Map.FindAndRemoveChecked(Key);
			}
		}
	}
	
	if(Preconditions.Num() > 0)
	{
		for(FWorldState World : Preconditions)
		{
			Preconditions_Map.Add(World.Key, World.Value);
		}
	}
	if(Effects.Num() > 0)
	{
		for(FWorldState World : Effects)
		{
			Effects_Map.Add(World.Key, World.Value);
		}
	}
	if(NewController)
		Controller = NewController;
	else
		UE_LOG(LogTemp, Error, TEXT("Pawn is null in %s initialization"), *ActionName.ToString())
}

bool UGActionBase::IsAchievable()
{
	return true;
}

bool UGActionBase::IsAchievableGivenPreconditions(TMap<FName, bool> Conditions)
{
	for(auto Precond : Preconditions_Map)
	{
		if(!Conditions.Contains(Precond.Key) || (Conditions.Contains(Precond.Key) && Precond.Value != Conditions[Precond.Key]))
			return false;
	}
	return true;
}

void UGActionBase::Act_Implementation()
{
}

void UGActionBase::CancelAction()
{
	bRunning=false;
	bIsInProgress=false;
	OnComplete.Clear();
	PostPerform(true);
}

void UGActionBase::TickAction_Implementation(float DeltaTime)
{
}


void UGActionBase::Tick(float DeltaTime)
{
	TickAction(DeltaTime);
}

bool UGActionBase::IsTickable() const
{
	return bShouldTick;
}

bool UGActionBase::IsTickableInEditor() const
{
	return false;
}

bool UGActionBase::IsTickableWhenPaused() const
{
	return false;
}

TStatId UGActionBase::GetStatId() const
{
	return TStatId();
}

UWorld* UGActionBase::GetWorld() const
{
	//Return null if the called from the CDO, or if the outer is being destroyed
	if (!HasAnyFlags(RF_ClassDefaultObject) && !GetOuter()->HasAnyFlags(RF_BeginDestroyed) && !GetOuter()->IsUnreachable())
	{
		//Try to get the world from the owning actor if we have one
		AActor* Outer = GetTypedOuter<AActor>();
		if (Outer != nullptr)
		{
			return Outer->GetWorld();
		}
	}
	//Else return null - the latent action will fail to initialize
	return nullptr;
}

bool UGActionBase::PostPerform_Implementation(bool bWasCanceled)
{
	return true;
}

bool UGActionBase::PrePerform_Implementation()
{
	return true;
}
