// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GActionBase.h"
#include "Kismet/KismetSystemLibrary.h"
#include "CoverCombatAction.generated.h"

/**
 * 
 */
UCLASS()
class AIRUNTIME_API UCoverCombatAction : public UGActionBase
{
	GENERATED_BODY()
private:

	FTimerHandle MoveOutHandle;
	FTimerHandle TakeCoverHandle;
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FVector> ChosenPositions;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MoveOffset = 50.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<EDrawDebugTrace::Type> LineTraceDrawType;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FVector2D MinMaxTimeToAttack = FVector2D(1,2);
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FVector2D MinMaxTimeToTakeCover = FVector2D(1,1.5f);
	
	
	virtual bool PrePerform_Implementation() override;
	virtual bool PostPerform_Implementation(bool bWasCanceled) override;
	virtual void Act_Implementation() override;
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintImplementableEvent)
	bool CheckOffsetPosition(FVector LeftOffset, FVector RightOffset, FVector HeightOffset, AActor* Enemy);

	UFUNCTION()
	void MoveOutOfCover();
	UFUNCTION()
	void GetBackToCover();

private:
	bool FindAttackAngle();
};
