// Fill out your copyright notice in the Description page of Project Settings.


#include "FindCoverAction.h"

#include "EnvironmentQuery/EnvQueryManager.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sandbox/AI/Controllers/SandboxGOAPController.h"
#include "Sandbox/Gameplay/Cover/SandboxCover.h"

bool UFindCoverAction::PrePerform_Implementation()
{
	if(!Controller || !Controller->GetPawn()) return false;
	QueryRequests = FEnvQueryRequest(FindCoverQuery, Controller->GetPawn());
	ACharacter* Character = Cast<ACharacter>(Controller->GetPawn());
	Character->UnCrouch();
	TArray<AActor*> Covers;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), TargetClass, Covers);
	for(auto* CoverActor : Covers)
	{
		if(auto* Cover = Cast<ASandboxCover>(CoverActor))
		{
			Cover->FreeCover(Controller->GetPawn());
		}
	}

	QueryRequests.Execute(EEnvQueryRunMode::RandomBest5Pct, this, &UFindCoverAction::OnQueryFinished);
	
	bWaitForEQS=true;
	UE_LOG(LogTemp, Warning, TEXT("Cover is %s"), SelectedCover ? TEXT("valid") : TEXT("null"));
	return true;
}

void UFindCoverAction::Act_Implementation()
{
	Super::Act_Implementation();
	if(bWaitForEQS) return;
	if(bIsInProgress || !Controller || !Controller->GetPawn()) return;
	UE_LOG(LogTemp, Warning, TEXT("xd, cover is %s"), *SelectedCover->GetName());
	if(auto* Movement = Cast<UCharacterMovementComponent>(Controller->GetPawn()->GetComponentByClass(UCharacterMovementComponent::StaticClass())))
	{
		Movement->StopActiveMovement();
		Controller->MoveToLocation(SelectedCoverLocation);
		if(!Controller->ReceiveMoveCompleted.Contains(this, "OnDestinationReached"))
			Controller->ReceiveMoveCompleted.AddDynamic(this, &UFindCoverAction::OnDestinationReached);
		bIsInProgress=true;
	}
}

bool UFindCoverAction::PostPerform_Implementation(bool bWasCanceled)
{
	if(Controller && Controller->GetPawn() && SelectedCover)
	{
		if(!bWasCanceled)
		{
			ASandboxGOAPController* AIController = Cast<ASandboxGOAPController>(Controller);
			AIController->CurrentCover = SelectedCover;
		}
		else
		{
			SelectedCover->FreeCover(Controller->GetPawn());
		}
	}
	bIsInProgress=false;
	ClosestDistance = 1000000;
	SelectedCover = nullptr;
	Target = nullptr;
	SelectedCoverLocation = FVector::Zero();
	return Super::PostPerform_Implementation(bWasCanceled);
}

void UFindCoverAction::OnDestinationReached(FAIRequestID ID, EPathFollowingResult::Type Result)
{
	if(Controller->ReceiveMoveCompleted.Contains(this, "OnDestinationReached"))
		Controller->ReceiveMoveCompleted.RemoveDynamic(this, &UFindCoverAction::OnDestinationReached);
	OnComplete.Broadcast();
	UE_LOG(LogTemp, Warning, TEXT("REACHED DESTINATION"));
}

void UFindCoverAction::OnQueryFinished(TSharedPtr<FEnvQueryResult> Result)
{
	AActor* Cover = Result->GetItemAsActor(0);
	bWaitForEQS = false;
	ACharacter* Char = Cast<ACharacter>(Controller->GetPawn());
	if(auto* NewCover = Cast<ASandboxCover>(Cover))
	{
		SelectedCover = NewCover;
		SelectedCoverLocation = SelectedCover->GetCoverLocations()[FMath::RandRange(0, SelectedCover->GetCoverLocations().Num() - 1)];
		SelectedCover->ReserveCover(SelectedCoverLocation, Char);
		Act();
	}
}
