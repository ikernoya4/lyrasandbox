// Fill out your copyright notice in the Description page of Project Settings.


#include "AIWorldState.h"

void UAIWorldState::ModifyState(FName Name, bool Value)
{
	if(GetWorldState().States.Num() > 0)
	{
		if(GetWorldState().HasState(Name))
		{
			GetWorldState().SetState(Name, Value);
		}	
	}
	OnStateChanged.Broadcast(Name, Value);
}
