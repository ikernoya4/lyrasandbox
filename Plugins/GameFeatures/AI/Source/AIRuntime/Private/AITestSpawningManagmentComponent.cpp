// Fill out your copyright notice in the Description page of Project Settings.


#include "AITestSpawningManagmentComponent.h"

#include "Character/LyraPawn.h"
#include "Teams/LyraTeamSubsystem.h"
#include "GameModes/LyraGameState.h"
#include "GameFramework/PlayerState.h"
#include "Player/LyraPlayerStart.h"
#include "Engine/World.h"
#include "Player/LyraPlayerController.h"

UAITestSpawningManagmentComponent::UAITestSpawningManagmentComponent(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

AActor* UAITestSpawningManagmentComponent::OnChoosePlayerStart(AController* Player,
                                                               TArray<ALyraPlayerStart*>& PlayerStarts)
{
	ULyraTeamSubsystem* TeamSubsystem = GetWorld()->GetSubsystem<ULyraTeamSubsystem>();
	const int32 PlayerTeamId = TeamSubsystem->FindTeamFromObject(Player);

	// We should have a TeamId by now, but early login stuff before post login can try to do stuff, ignore it.
	if (!ensure(PlayerTeamId != INDEX_NONE))
	{
		return nullptr;
	}

	ALyraGameState* GameState = GetGameStateChecked<ALyraGameState>();

	ALyraPlayerStart* BestPlayerStart = nullptr;
	double MaxDistance = 0;
	ALyraPlayerStart* FallbackPlayerStart = nullptr;
	double FallbackMaxDistance = 0;
	const APlayerController* PlayerController = Cast<APlayerController>(Player);
	bool bIsBot = PlayerController ? false : true;

	for (APlayerState* PS : GameState->PlayerArray)
	{
		const int32 TeamId = TeamSubsystem->FindTeamFromObject(PS);
		
		// We should have a TeamId by now...
		if (PS->IsOnlyASpectator() || !ensure(TeamId != INDEX_NONE))
		{
			continue;
		}

		// If the other player isn't on the same team, lets find the furthest spawn from them.
		if (TeamId != PlayerTeamId)
		{
			for (ALyraPlayerStart* PlayerStart : PlayerStarts)
			{
				if (APawn* Pawn = PS->GetPawn())
				{
					const double Distance = PlayerStart->GetDistanceTo(Pawn);

					if (PlayerStart->IsClaimed())
					{
						if (FallbackPlayerStart == nullptr || Distance > FallbackMaxDistance)
						{
							FallbackPlayerStart = PlayerStart;
							FallbackMaxDistance = Distance;
						}
					}
					else if (PlayerStart->GetLocationOccupancy(Player) < ELyraPlayerStartLocationOccupancy::Full)
					{
						if(bIsBot && PlayerStart->PlayerStartTag == BotStartTag)
						{
							if (BestPlayerStart == nullptr)
							{
								BestPlayerStart = PlayerStart;
								MaxDistance = Distance;
							}
						}
						if(!bIsBot && PlayerStart->PlayerStartTag == PlayerStartTag)
						{
							if (BestPlayerStart == nullptr)
							{
								BestPlayerStart = PlayerStart;
								MaxDistance = Distance;
							}
						}
						
					}
				}
			}
		}
		else if(!bIsBot) // Forcing the player to spawn in a specific place on the test map
		{
			for (auto* Start : PlayerStarts)
			{
				if (Start->PlayerStartTag == PlayerStartTag && BestPlayerStart == nullptr)
				{
					BestPlayerStart = Start;
				}
			}
		}
	}

	if (BestPlayerStart)
	{
		return BestPlayerStart;
	}

	return FallbackPlayerStart;
}

void UAITestSpawningManagmentComponent::OnFinishRestartPlayer(AController* Player, const FRotator& StartRotation)
{
}
