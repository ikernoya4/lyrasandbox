// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/PlannerComponent.h"

#include "GeometryCollection/GeometryCollectionAlgo.h"

// ------------------------------------- PLANNER ---------------------------------------


void UPlannerComponent::BeginPlay()
{
	Super::BeginPlay();
}

UPlannerComponent::UPlannerComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

bool UPlannerComponent::GoalAchieved(TMap<FName, bool> GoalToCheck, TMap<FName, bool> State)
{
	for(auto G : GoalToCheck)
	{
		if(!State.Contains(G.Key))
			return false;	
	}
	return true;
}

TArray<UGActionBase*> UPlannerComponent::ActionSubset(TArray<UGActionBase*> Actions, UGActionBase* SelectedAction)
{
	TArray<UGActionBase*> Subset;
	for(UGActionBase* A : Actions)
	{
		if(A->ActionName != SelectedAction->ActionName)
			Subset.Add(A);
	}
	return Subset;
}

bool UPlannerComponent::BuildGraph(UGNode* Parent, TArray<UGNode*> &Leaves, TArray<UGActionBase*> GoapActions,
                                   TMap<FName, bool> FromGoal)
{
	bool bFoundPath = false;
	for(UGActionBase* Action : GoapActions)
	{
		if(Action->IsAchievableGivenPreconditions(Parent->State))
		{
			TMap<FName, bool> CurrentState;
			for(auto Effects : Action->Effects)
			{
				if(!CurrentState.Contains(Effects.Key))
				{
					CurrentState.Add(Effects.Key, Effects.Value);
				}
			}
			UGNode* Node = NewObject<UGNode>();
			Node->Initialize(Parent, Parent->Cost + Action->Cost, CurrentState, Action);
			if(GoalAchieved(FromGoal, CurrentState))
			{
				Leaves.Add(Node);
				bFoundPath = true;
			}
			else
			{
				TArray<UGActionBase*> Subset = ActionSubset(GoapActions, Action);
				bool bFound = BuildGraph(Node, Leaves, Subset, FromGoal);
				if(bFound)
					bFoundPath=true;
			}
		}
	}
	return bFoundPath;
}

TArray<UGActionBase*> UPlannerComponent::Plan(TArray<UGActionBase*> Actions, TMap<FName, bool> Goal, FWorldStates States)
{
	TArray<FName> Keys;
	States.States.GetKeys(Keys);
	for(FName Key : Keys)
	{
		if(Goal.Contains(Key) && Goal[Key] == States.States[Key])
			return TArray<UGActionBase*>();
	}
	
	// Creates a list of usable actions from the given array
	TArray<UGActionBase*> UsableActions;
	for(UGActionBase* Act : Actions)
	{
		if(Act->IsAchievable())
		{
			UsableActions.Add(Act);
		}
	}

	//Start Creating the sequence of actions
	TArray<UGNode*> Leaves;
	UGNode* Start = NewObject<UGNode>();
	Start->Initialize(nullptr, 0, States.GetStates(),nullptr);
	bool Success = BuildGraph(Start, Leaves, UsableActions, Goal);
	if(!Success)
	{
		UE_LOG(LogTemp, Warning, TEXT("Couldn't Create a Plan"));
		return TArray<UGActionBase*>();
	}

	//Get the Cheapest node in the sequence
	UGNode* Cheapest = nullptr;
	for (auto Leaf : Leaves)
	{
		if(Cheapest == nullptr)
			Cheapest = Leaf;
		else
			if(Leaf->Cost < Cheapest->Cost)
				Cheapest = Leaf;
	}

	//Reorganize and filter actions
	TArray<UGActionBase*> Result;
	UGNode* N = Cheapest;
	while (N!=nullptr)
	{
		if(N->Action)
		{
			Result.Insert(N->Action, 0);
		}
		N = N->Parent;
	}

	//Create Sequence of actions in order
	TArray<UGActionBase*> Queue;
	for(UGActionBase* A : Result)
	{
		Queue.Emplace(A);
	}
	UE_LOG(LogTemp, Warning, TEXT("THE PLAN IS:"))
	for(UGActionBase* A : Queue)
	{
		UE_LOG(LogTemp, Warning, TEXT("%s, "), *A->ActionName.ToString())
	}
	bShouldFormulatePlan=false;
	return Queue;
}


// ------------------------------------- NODE ---------------------------------------
void UGNode::Initialize(UGNode* NewParent, float NewCost, TMap<FName, bool> AllStates, UGActionBase* NewAction)
{
	Parent = NewParent;
	Cost = NewCost;
	State = AllStates;
	Action = NewAction;
}