﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "AIRuntime/Public/Components/ActionsComponent.h"

#include "AIController.h"


UActionsComponent::UActionsComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UActionsComponent::InitializeActions(UAgentData* AgentData)
{
	if(AgentData && Actions.Num() <= 0)
	{
		for(auto ActionClass : AgentData->Actions)
		{
			if(ActionClass)
			{
				if (UGActionBase* Action = NewObject<UGActionBase>(this, ActionClass))
				{
					Actions.Add(Action);
				}
			}
		}
	}
	if(auto* Controller = Cast<AAIController>(GetOwner()))
	{
		if(Controller->GetPawn())
		{
			for (UGActionBase* Action : Actions	)
			{
				if (Action)
				{
					Action->Init(Controller);
				}
			}
		}
	}
}

void UActionsComponent::ResetActions()
{
	if(Actions.Num() > 0)
	{
		for(auto* Action : Actions)
		{
			if(Action)
			{
				Action->CancelAction();
			}
		}
	}
}
