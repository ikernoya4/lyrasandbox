// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/GoapComponent.h"

#include "AIController.h"

void UGoapComponent::BeginPlay()
{
	Super::BeginPlay();
	if(auto* Controller = Cast<AAIController>(GetOwner()))
	{
		OwnerController = Controller;
	}
	if(auto* WState = Cast<UAIWorldState>(GetWorld()->GetGameState()->GetComponentByClass(UAIWorldState::StaticClass())))
	{
		WorldState = WState;
	}
	else
		UE_LOG(LogTemp, Error,TEXT("Couldn't Get WorldState in GameState"))
}

UGoapComponent::UGoapComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UGoapComponent::Initialize(UActionsComponent* ActionsComp, UPlannerComponent* PlannerComp)
{
	if(!ActionsComp || !PlannerComp || !AgentData || bIsInitialized) return;
	ActionsComponent = ActionsComp;
	Planner = PlannerComp;
	AAIController* Controller = Cast<AAIController>(GetOwner());
	if(!Controller || !Controller->GetPawn()) return;
	ActionsComponent->InitializeActions(AgentData);
	Goals = AgentData->Goals;
	bIsInitialized=true;
}

void UGoapComponent::CancelAndReformulatePlan()
{
	CancelActions();
	ActionQueue.Empty();
	CurrentAction = nullptr;
	GetWorld()->GetTimerManager().SetTimer(DelayHandle, this, &UGoapComponent::AllowPlanningAfterDelay, 0.2f);
}

void UGoapComponent::CancelActions()
{
	if(ActionsComponent)
	{
		ActionsComponent->ResetActions();
	}
}

void UGoapComponent::Reset()
{
	AgentBeliefs = InitialAgentBeliefs;
	CurrentAction = nullptr;
	CurrentGoal = FSubGoal();
}

void UGoapComponent::FormulatePlan()
{
	if(!Planner) return;
	if(Planner->bShouldFormulatePlan)
	{
		for(auto G : Goals)
		{
			ActionQueue = Planner->Plan(ActionsComponent->Actions, G.Key.Goals, AgentBeliefs);
			if(!ActionQueue.IsEmpty())
			{
				CurrentGoal = G.Key;
				break;
			}
		}
	}
}

void UGoapComponent::CheckCurrentGoalCompletion()
{
	if(ActionQueue.IsEmpty())
	{
		if(CurrentGoal.bRemove)
		{
			Goals.Remove(CurrentGoal);
		}
		if(Goals.Num() <= 0)
		{
			CurrentGoal.ClearGoal();
		}
		Planner->bShouldFormulatePlan=true;
	}	
}

void UGoapComponent::StartCurrentAction()
{
	if(ActionQueue.Num() > 0)
	{
		CurrentAction = ActionQueue[0];
		ActionQueue.RemoveAt(0);
		if(CurrentAction && CurrentAction->PrePerform())
		{
			UE_LOG(LogTemp, Warning, TEXT("Started %s"), *CurrentAction->ActionName.ToString())
			CurrentAction->bRunning = true;
			CurrentAction->OnComplete.AddDynamic(this, &UGoapComponent::OnActionComplete);
			CurrentAction->Act();
		}
	}
}

void UGoapComponent::SetBeliefState(FName State, bool Value)
{
	if(AgentBeliefs.States.Contains(State))
	{
		AgentBeliefs.States[State] = Value;
	}
}



void UGoapComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if(!Planner || !ActionsComponent || bIsAgentDead)return;
	if(CurrentAction)
	{
		if(CurrentAction->bRunning)
		{
			// CurrentAction->Act();
			return;
		}
	}
	FormulatePlan();
	CheckCurrentGoalCompletion();
	StartCurrentAction();
}

void UGoapComponent::OnActionComplete()
{
	if(!CurrentAction)return;
	UE_LOG(LogTemp, Warning, TEXT("Completed Action"))
	CurrentAction->bRunning = false;
	CurrentAction->PostPerform(false);
	CurrentAction->bIsInProgress = false;
	for(int i = 0; i < AgentBeliefs.States.Num(); i++)
	{
		for(auto Effect : CurrentAction->Effects)
		{
			if(GetBeliefs().States.Contains(Effect.Key))
			{
				SetBeliefState(Effect.Key, Effect.Value);
			}
		}
	}
	if(CurrentAction->OnComplete.Contains(this, "OnActionComplete"))
	{
		CurrentAction->OnComplete.RemoveDynamic(this, &UGoapComponent::OnActionComplete);
	}
}

void UGoapComponent::CompareBeliefsWithWorldState(FName Name, bool Value)
{
	if(AgentBeliefs.HasState(Name) && AgentBeliefs.States[Name] != Value)
	{
		AgentBeliefs.SetState(Name, Value);
	}
}

void UGoapComponent::AllowPlanningAfterDelay()
{
	Planner->bShouldFormulatePlan=true;
	GetWorld()->GetTimerManager().ClearTimer(DelayHandle);
	Goals = AgentData->Goals;
}

