// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Player/LyraPlayerSpawningManagerComponent.h"
#include "AITestSpawningManagmentComponent.generated.h"

/**
 * 
 */
UCLASS()
class UAITestSpawningManagmentComponent final : public ULyraPlayerSpawningManagerComponent
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AITest|Spawner")
	FName PlayerStartTag = "Player";
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AITest|Spawner")
	FName BotStartTag = "Bot";

	
	UAITestSpawningManagmentComponent(const FObjectInitializer& ObjectInitializer);
	virtual AActor* OnChoosePlayerStart(AController* Player, TArray<ALyraPlayerStart*>& PlayerStarts) override;
	virtual void OnFinishRestartPlayer(AController* Player, const FRotator& StartRotation) override;
	
};
