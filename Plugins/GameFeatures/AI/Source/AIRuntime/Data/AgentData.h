// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GoapStructs.h"
#include "AIRuntime/Actions/GActionBase.h"
#include "Engine/DataAsset.h"
#include "AgentData.generated.h"

/**
 * 
 */
UCLASS()
class AIRUNTIME_API UAgentData : public UPrimaryDataAsset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	FName Name;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	TArray<TSubclassOf<UGActionBase>> Actions;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	TMap<FSubGoal, bool> Goals;
};
