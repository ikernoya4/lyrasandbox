﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "UObject/NoExportTypes.h"
#include "Misc/Crc.h"
#include "GoapStructs.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FWorldState
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName Key;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool Value;

	bool operator==(const FWorldState& Other) const;
	
};

USTRUCT(BlueprintType)
struct FWorldStates
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WorldState")
	TMap<FName, bool> States;

	FWorldStates()
	{
		if(States.Num()>0)
			States.Empty();
	}
	
	void ModifyState(FName Key, bool Value);

	void RemoveState(FName Key);

	void SetOrAddState(FName Key, bool Value);
	void SetState(FName Key, bool Value);

	FORCEINLINE TMap<FName, bool> GetStates() const {return States;}

	void AddState(FName Key, bool Value){ States.Add(Key, Value); }
	
	
	FORCEINLINE bool HasState(FName Key) const {return States.Contains(Key);}

	bool operator==(const FWorldStates& Other) const;
	
};

USTRUCT(BlueprintType)
struct FGoapAction
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName ActionName = NAME_None;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayTag ActionTag;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Cost = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AActor* Target = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Duration = 0;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FWorldState> Preconditions;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FWorldState> Effects;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FWorldStates AgentBeliefs;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	bool bRunning = false;
private:
	TMap<FName, bool> Preconditions_Map;
	TMap<FName, bool> Effects_Map;

public:
	FGoapAction(){}
	void Initialize();
	bool IsAchievable();
	bool IsAchievableGivenPreconditions(TMap<FName, bool> Conditions);
	//Right now, Having a gameplay tag is essential
	bool IsInitialized();
	bool Equals(FGoapAction& Other);
	
};

USTRUCT(BlueprintType)
struct FSubGoal
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName Name = "Goal";
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<FName, bool> Goals;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bRemove = true;
	
	FSubGoal();
	
	FSubGoal(FName Key, bool Value, bool bIsRemovable);

	bool operator==(const FSubGoal& Other) const;

	bool IsValid() {return Name != NAME_None && Goals.IsEmpty();}
	void ClearGoal();

	
};
FORCEINLINE uint32 GetTypeHash(const FSubGoal& This)
{
	const uint32 Hash = FCrc::MemCrc32(&This, sizeof(FSubGoal));
	return Hash;
}
