﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "GoapStructs.h"

bool FWorldState::operator==(const FWorldState& Other) const
{
	return Key == Other.Key && Value == Other.Value;
}

// --------------------------------------------- World States ---------------------------------------------
void FWorldStates::ModifyState(FName Key, bool Value)
{
	if(States.Contains(Key))
	{
		bool bPrevState = States[Key];
		States[Key] = Value;
		if(bPrevState != States[Key])
		{
			RemoveState(Key);
		}
	}
	else
	{
		States.Add(Key, Value);
	}
}

void FWorldStates::RemoveState(FName Key)
{
	if(States.Contains(Key))
	{
		States.Remove(Key);
	}
}

void FWorldStates::SetOrAddState(FName Key, bool Value)
{
	if(States.Contains(Key))
	{
		States[Key] = Value;
	}
	else
	{
		States.Add(Key, Value);
	}
}

void FWorldStates::SetState(FName Key, bool Value)
{
	if(States.Contains(Key))
	{
		States[Key] = Value;
	}
}

bool FWorldStates::operator==(const FWorldStates& Other)const
{
	return States.Num() == Other.States.Num();
}

// --------------------------------------------- Action ---------------------------------------------
void FGoapAction::Initialize()
{
	if(Preconditions.Num() > 0)
	{
		for(FWorldState World : Preconditions)
		{
			Preconditions_Map.Add(World.Key, World.Value);
		}
	}
	if(Effects.Num() > 0)
	{
		for(FWorldState World : Effects)
		{
			Effects_Map.Add(World.Key, World.Value);
		}
	}
}

bool FGoapAction::IsAchievable()
{
	return true;
}

bool FGoapAction::IsAchievableGivenPreconditions(TMap<FName, bool> Conditions)
{
	for(auto Condition : Preconditions_Map)
	{
		if(!Conditions.Contains(Condition.Key))
			return false;
	}
	return true;
}

bool FGoapAction::IsInitialized()
{
	return ActionTag.IsValid() && ActionName != NAME_None;
}

bool FGoapAction::Equals(FGoapAction& Other)
{
	return (ActionName == Other.ActionName) && (ActionTag == Other.ActionTag) && (Cost == Other.Cost) &&
		(Target == Other.Target) && (Duration == Other.Duration) && (Preconditions == Other.Preconditions) &&
			(Effects == Other.Effects) && (AgentBeliefs == Other.AgentBeliefs);
}

// --------------------------------------------- GOALS ---------------------------------------------

FSubGoal::FSubGoal(){}

FSubGoal::FSubGoal(FName Key, bool Value, bool bIsRemovable)
{
	Goals.Add(Key, Value);
	bRemove = bIsRemovable;
}

bool FSubGoal::operator==(const FSubGoal& Other) const
{
	return Name == Other.Name;
}

void FSubGoal::ClearGoal()
{
	Name = NAME_None;
	Goals.Empty();
}
	