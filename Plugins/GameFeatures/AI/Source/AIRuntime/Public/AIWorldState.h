// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIRuntime/Data/GoapStructs.h"
#include "Components/GameStateComponent.h"
#include "AIWorldState.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnStateChanged, FName, StateName, bool, Value);

UCLASS()
class AIRUNTIME_API UAIWorldState : public UGameStateComponent
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FWorldStates WorldState;

public:
	UPROPERTY(BlueprintAssignable, BlueprintCallable)
	FOnStateChanged OnStateChanged;
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	FORCEINLINE FWorldStates GetWorldState() const {return WorldState;}
	UFUNCTION(BlueprintCallable)
	void ModifyState(FName Name, bool Value);
};
