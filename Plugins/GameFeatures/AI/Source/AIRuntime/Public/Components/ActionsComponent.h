﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIRuntime/Actions/GActionBase.h"
#include "AIRuntime/Data/AgentData.h"
#include "Components/ActorComponent.h"
#include "ActionsComponent.generated.h"


UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable, BlueprintType)
class AIRUNTIME_API UActionsComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly)
	TArray<UGActionBase*> Actions;

	UFUNCTION(BlueprintCallable)
	void InitializeActions(UAgentData* Data);
	UFUNCTION(BlueprintCallable)
	void ResetActions();

	UActionsComponent();
};
