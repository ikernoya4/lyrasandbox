// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIWorldState.h"
#include "AIRuntime/Actions/GActionBase.h"
#include "AIRuntime/Data/GoapStructs.h"
#include "Components/ActorComponent.h"
#include "PlannerComponent.generated.h"


class UGNode;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), BlueprintType, Blueprintable)
class AIRUNTIME_API UPlannerComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	bool bShouldFormulatePlan = true;
	
protected:

	virtual void BeginPlay() override;
public:
	UPlannerComponent();

	bool GoalAchieved(TMap<FName, bool> GoalToCheck, TMap<FName, bool> State);
	TArray<UGActionBase*> ActionSubset(TArray<UGActionBase*> Actions, UGActionBase* SelectedAction);
	bool BuildGraph(UGNode* Parent, TArray<UGNode*> &Leaves, TArray<UGActionBase*> GoapActions, TMap<FName, bool> FromGoal);
	UFUNCTION(BlueprintCallable)
	TArray<UGActionBase*> Plan(TArray<UGActionBase*> Actions, TMap<FName, bool> Goal, FWorldStates States);
	
		
};

// USTRUCT(BlueprintType)
// struct FGNode
// {
// 	GENERATED_BODY()
// 	
// 	FGNode Parent;
// 	
// 	UPROPERTY(EditAnywhere, BlueprintReadWrite)
// 	float Cost = 0;
// 	UPROPERTY(EditAnywhere, BlueprintReadWrite)
// 	TMap<FName, bool> State;
// 	UPROPERTY(EditAnywhere, BlueprintReadWrite)
// 	FGoapAction Action;
//
// 	FGNode(){}
// 	FGNode(FGNode& NewParent, float NewCost, TMap<FName, bool> AllStates, FGoapAction NewAction);
// 	bool IsNull();
// 	void operator=(const FGNode& Other) 
// 	{
// 		if(!Other.Parent.IsNull()) Parent = Other.Parent;
// 		Cost = Other.Cost;
// 		State = Other.State;
// 		Action = Other.Action;
// 	}
// };

UCLASS(BlueprintType, Blueprintable)
class UGNode : public UObject
{
	GENERATED_BODY()
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UGNode* Parent = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Cost = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<FName, bool> State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UGActionBase* Action;

	UGNode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer){}
	void Initialize(UGNode* NewParent, float NewCost, TMap<FName, bool> AllStates, UGActionBase* NewAction);
};
