// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ActionsComponent.h"
#include "AIController.h"
#include "PlannerComponent.h"
#include "Components/ActorComponent.h"
#include "GoapComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class AIRUNTIME_API UGoapComponent : public UActorComponent
{
	GENERATED_BODY()
	
	UPROPERTY()
	UActionsComponent* ActionsComponent;
	UPROPERTY()
	UPlannerComponent* Planner;
	UPROPERTY()
	AAIController* OwnerController;
	UPROPERTY()
	UAIWorldState* WorldState;

	FTimerHandle DelayHandle;
	
	bool bIsInitialized = false;
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	UAgentData* AgentData;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	FWorldStates AgentBeliefs;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "AI")
	FWorldStates InitialAgentBeliefs;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|Settings")
	UGActionBase* CurrentAction = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|Settings")
	FSubGoal CurrentGoal;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|Settings")
	TArray<UGActionBase*> ActionQueue;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|Settings")
	TMap<FSubGoal, bool> Goals;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "AI")
	bool bIsAgentDead = false;
	
	virtual void BeginPlay() override;
	
public:	
	// Sets default values for this component's properties
	UGoapComponent();

	UFUNCTION(BlueprintCallable)
	void Initialize(UActionsComponent* ActionsComp, UPlannerComponent* PlannerComp);

	UFUNCTION(BlueprintCallable)
	void CancelAndReformulatePlan();

	UFUNCTION(BlueprintCallable)
	void CancelActions();
	UFUNCTION(BlueprintCallable)
	void Reset();
	UFUNCTION(BlueprintCallable)
	void FormulatePlan();
	UFUNCTION(BlueprintCallable)
	void CheckCurrentGoalCompletion();
	UFUNCTION(BlueprintCallable)
	void StartCurrentAction();
	UFUNCTION(BlueprintCallable)
	void SetBeliefState(FName State, bool Value);
	UFUNCTION(BlueprintCallable)
	FORCEINLINE FWorldStates GetBeliefs() const {return AgentBeliefs;}


public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:

	UFUNCTION()
	void OnActionComplete();
	UFUNCTION()
	void CompareBeliefsWithWorldState(FName Name, bool Value);
	UFUNCTION()
	void AllowPlanningAfterDelay();
};
